#import <Foundation/Foundation.h>

typedef void(^completion)(BOOL,NSDictionary*);

//custom alert

#define kCustomAlertWithParamAndTarget(title,msg,target) [[[UIAlertView alloc] initWithTitle:title message:msg delegate:target cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show]

#define BASEURL @"http://main.spaceotechnologies.com/qd/api/v1"
#define DisplayAlertWithTitle(msg,title) {UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]; [alertView show];}

#define DisplayAlertControllerWithTitle(msg,title) {UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert]; UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { }]; [alert addAction:defaultAction]; [self presentViewController:alert animated:YES completion:nil];} // 11

#define RGBCOLOR(r,g,b)[UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define SharedObj   [SharedClass objSharedClass]
#define   appDelegate           (AppDelegate *)[[UIApplication sharedApplication] delegate]

#define kFixedDistanceFilter 10;  // Distance Filer

//Value
#define XGET_VALUE(v) [[NSUserDefaults standardUserDefaults] valueForKey:v]
#define XSET_VALUE(k,v) [[NSUserDefaults standardUserDefaults] setValue:v forKey:k];

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

@interface Common : NSObject

+(void)postServiceWithURL:(NSString*)strUrl withParam:(NSDictionary*)dictParam withCompletion:(void(^)(NSDictionary*response,BOOL success1))completion;
+(void)getServiceWithURL:(NSString*)strUrl withParam:(NSDictionary*)dictParam withCompletion:(void(^)(NSDictionary*response,BOOL success1))completion;

+(BOOL)validateEmail:(NSString*)email;
+(BOOL)validatePassword:(NSString *)pwd;
+(BOOL)isNetworkReachable;
+(BOOL)validateUrl:(NSString *)strURL;
+(BOOL)isValidPinCode:(NSString*)pincode;

+(NSDictionary *)postRequest :(NSString *)url post:(NSString *)post;
+(void)callWebService:(NSString *)post JSONstring:(NSString *)strJSON  completion:(completion) compblock;
+(NSString*)GetTokenWithNonce:(NSString *)nonce Timestamp:(NSString *)timestamp;

+(void)callGetNotificationcount:(void(^)(int notificationCount))completion2;
+(void)updateBadgeCount;

@end

//
//  DrawerVC.m
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "DrawerVC.h"
#import "MMNavigationController.h"

@interface DrawerVC ()

@end

@implementation DrawerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    UIViewController *leftSideDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SideVC"];
//    UIViewController *centerDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainScreen"];
//    
//    self.centerViewController = centerDrawerViewController;
//    
//    self.leftDrawerViewController = leftSideDrawerViewController;
//    self.maximumLeftDrawerWidth = 280;
//    self.maximumRightDrawerWidth = 300;
//    
//    self.openDrawerGestureModeMask = MMOpenDrawerGestureModeNone;
//    
//    [self setShowsShadow:NO];
//    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
//    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
//    [self.navigationController setNavigationBarHidden:YES];
    
//    [self.navigationController setNavigationBarHidden:NO];
//    
////    [self setLeftBtnInListVIewVC:@"sidebar" andLSelector:@selector(sideBtnClk:)];
//    [self setTitle:@"Q'd"];
//    //    [self setRightBtninListViewVCWithSelector:@selector(mapViewBtnClick:)];
//    
//    
//    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    btnRight.bounds = CGRectMake(self.view.frame.size.width-90, 20, 82, 44);
////    [btnRight addTarget:self action:@selector(mapViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [btnRight setTitle:@"MAP VIEW" forState:UIControlStateNormal];
//    [btnRight.titleLabel setFont:[UIFont fontWithName:@"Hind-Light" size:18.0f]];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
//    self.navigationItem.rightBarButtonItem = backButton;
//    self.navigationController.navigationBar.barTintColor = RGBCOLOR(243, 67, 95);
//    self.navigationController.navigationBar.translucent = NO;

    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
//    UIImageView *img = [[UIImageView alloc] initWithFrame:self.view.bounds];
//    img.image = [UIImage imageNamed:@"SplashScreen"];
//    [self.view addSubview:img];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
        UIViewController *leftSideDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SideVC"];
        UIViewController *centerDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainScreen"];
        
        // Added on 25-Jan
        UINavigationController * navigationController = [[MMNavigationController alloc] initWithRootViewController:centerDrawerViewController];
        [navigationController setRestorationIdentifier:@"MMExampleCenterNavigationControllerRestorationKey"];
        
        
        UINavigationController * leftSideNavController = [[MMNavigationController alloc] initWithRootViewController:leftSideDrawerViewController];
        [leftSideNavController setRestorationIdentifier:@"MMExampleLeftNavigationControllerRestorationKey"];
        
        //    self.drawerController = [[MMDrawerController alloc]
        //                             initWithCenterViewController:navigationController
        //                             leftDrawerViewController:leftSideNavController
        //                             rightDrawerViewController:rightSideNavController];
        
        
        // till here on 25-Jan
        
        //    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        //    }
        
        self.centerViewController = navigationController;
        
        self.leftDrawerViewController = leftSideDrawerViewController;
        
        if (IS_IPHONE_4_OR_LESS) {
            self.maximumLeftDrawerWidth = 280;
        }
        else if (IS_IPHONE_5) {
            self.maximumLeftDrawerWidth = 280;
        }
        else if (IS_IPHONE_6)
        {
            self.maximumLeftDrawerWidth = 330;
        }
        else
        {
            self.maximumLeftDrawerWidth = 370;
        }
        
        self.maximumRightDrawerWidth = 300;
        
        self.openDrawerGestureModeMask = MMOpenDrawerGestureModeNone;
        
        [self setShowsShadow:NO];
        [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
//    });
   
}

-(void)viewDidAppear:(BOOL)animated
{

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

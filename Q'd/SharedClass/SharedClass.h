//
//  SharedClass.h
//  Qd
//
//  Created by SOTSYS028 on 12/01/16.
//  Copyright © 2016 SOTSYS028. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedClass : NSObject
{

}
@property (nonatomic,assign) BOOL hasLogin;
@property (nonatomic,assign) BOOL hasJoinedQueue; // commented on 29-3

@property (nonatomic,strong) NSString *mapViewBtnTitleLblText;
@property (nonatomic,assign) BOOL isSlowModeOn;

//
@property (nonatomic,strong) NSMutableDictionary *joinInQueueParameterDict;
//@property (nonatomic) int hasJoinedQueueStatus; // Added on 29-3
@property (nonatomic,assign) BOOL isSeated;
@property (nonatomic) int restaurandID;     // when click on any restaurant , assign restaurantID to this variable
@property (nonatomic,strong) NSMutableDictionary *notificationDict; // to check for user has seated in particular restaurant after receiving push every 15min use this variable

+(SharedClass *)objSharedClass;

@end

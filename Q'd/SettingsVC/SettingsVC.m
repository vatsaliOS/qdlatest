//
//  SettingsVC.m
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingsCell.h"
#import "SettingsCell.h"
#import "MBProgressHUD.h"


@interface SettingsVC ()
{
    BOOL isFiltered;
    NSMutableArray *getRestaurantListArray;
    NSMutableArray *tmpArray;
    
    NSMutableArray *tmpRestaurantArray;
    
    int venuesCount;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    self.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 10.0f)];

    pageOffset = 1;
    
    // For Search
    isSearchClicked = NO;
    getRestaurantListArray = [[NSMutableArray alloc]init];
    
    tmpRestaurantArray = [NSMutableArray new];
    
    [self VenuesAPI];
}

-(void)dismissKeyboard {
    [self.searchBar resignFirstResponder];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
    [self setLeftBtnWithImageName:@"Back" andLSelector:@selector(backBtnClk:)];   // Use this when using custom Navigation bar

    CGRect frame = CGRectMake(120, 0, 50, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HindVadodara-Bold" size:18.0f];
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        label.font = [UIFont fontWithName:@"HindVadodara-Bold" size:18.0f];
    }
    else if (IS_IPHONE_6 || IS_IPHONE_6P)
    {
        label.font = [UIFont fontWithName:@"HindVadodara-Bold" size:22.0f];
    }
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"SETTINGS";
    self.navigationItem.titleView = label;

    self.navigationController.navigationBar.barTintColor = RGBCOLOR(255, 0, 78);
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

#pragma mark - GetRestaurantListAPI

-(void)VenuesAPI
{
    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%d",pageOffset] forKey:@"page"];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetails"]valueForKey:@"user_id"]] forKey:@"user_id"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/get_slow_time_alerts_venues/",BASEURL] withParam:Dict withCompletion:^(NSDictionary *response, BOOL success){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            [getRestaurantListArray addObjectsFromArray:[response valueForKey:@"venues"]];
            NSLog(@"venues_count == %d",[[response valueForKey:@"venues_count"] intValue]);
            
            tmpRestaurantArray = [getRestaurantListArray mutableCopy];
            
            venuesCount = [[response valueForKey:@"venues_count"] intValue];
            
            if ([[response valueForKey:@"venues_count"] intValue]%5 >= 0) {
                isMorePage=YES;
            }
            else
            {
                isMorePage=NO;
            }
            
            [self.tblView reloadData];
            isPageRefresing=NO;
            
        }
        else
        {
            
        }
    }];
    
}


#pragma mark - Back Clicked
- (IBAction)backBtnClk:(id)sender {
    [self.searchBar resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowFadeImg" object:nil];
}

#pragma mark TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return getRestaurantListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsCell *cell = (SettingsCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.backgroundColor = [UIColor whiteColor];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[getRestaurantListArray objectAtIndex:indexPath.row]valueForKey:@"image"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    cell.titleLbl.text = [NSString stringWithFormat:@"%@",[[getRestaurantListArray objectAtIndex:indexPath.row]valueForKey:@"name"]];
    cell.addressLbl.text = [NSString stringWithFormat:@"%@",[[getRestaurantListArray objectAtIndex:indexPath.row]valueForKey:@"address"]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.onOffBtn.tag = indexPath.row;
    [cell.onOffBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[[getRestaurantListArray objectAtIndex:indexPath.row] valueForKey:@"slowmode"] isEqualToNumber:[NSNumber numberWithInt:0]]) {
        [cell.onOffBtn setBackgroundImage:[UIImage imageNamed:@"OffBtn"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.onOffBtn setBackgroundImage:[UIImage imageNamed:@"OnBtn"] forState:UIControlStateNormal];
    }
    
    if (IS_IPHONE_4_OR_LESS){
        [cell.addressLbl setFont:[UIFont fontWithName:@"HindVadodara-Light" size:14.0]];
    }
    else if (IS_IPHONE_5){
        [cell.addressLbl setFont:[UIFont fontWithName:@"HindVadodara-Light" size:15.0]];
    }
    else
    {
        [cell.addressLbl setFont:[UIFont fontWithName:@"HindVadodara-Light" size:17.0]];
    }

    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchBar resignFirstResponder];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5){
        return 60;
    }
    else
    {
        return 75;
    }

}

#pragma mark - SlowTimeAlert Click

-(IBAction)btnClick:(UIButton*)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SettingsCell *cell = [self.tblView cellForRowAtIndexPath:indexPath];
    
    UIImage *btnBgImg = cell.onOffBtn.currentBackgroundImage;
    
    UIImage *compareWithImg = [UIImage imageNamed:@"OnBtn"];
    
    BOOL isSlowTimeAlert;
    if ([btnBgImg isEqual:compareWithImg]) {
        [cell.onOffBtn setBackgroundImage:[UIImage imageNamed:@"OffBtn"] forState:UIControlStateNormal];
        isSlowTimeAlert = FALSE;
    }
    else
    {
        [cell.onOffBtn setBackgroundImage:[UIImage imageNamed:@"OnBtn"] forState:UIControlStateNormal];
        isSlowTimeAlert = TRUE;

    }
    
    [self updateSlowTimeAlertAPI:isSlowTimeAlert andRestaurantID:[sender tag]];

//    if (cell.onOffBtn.isSelected) {
//        [sender setBackgroundImage:[UIImage imageNamed:@"OnBtn"] forState:UIControlStateNormal];
//        [sender setSelected: NO];
//    }
//    else
//    {
//        [sender setBackgroundImage:[UIImage imageNamed:@"OffBtn"] forState:UIControlStateNormal];
//        [sender setSelected:YES];
//    }
}

#pragma mark - ScrollView Delegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height)
    {
        if (!isPageRefresing)
        {
            isPageRefresing=YES;
            pageOffset= pageOffset+1;
            if (isMorePage)
            {
                if (isSearchClicked) {
                    
                    if (getRestaurantListArray.count < venuesCount) {
                        [self searchAPI];
                    }
                    
                }
                else
                {
                    if (getRestaurantListArray.count < venuesCount) {
                        [self VenuesAPI];
                    }
                }

            }
        }
        // we are at the end
    }
    
}

#pragma mark - SlowTimeAlert Click

-(void)updateSlowTimeAlertAPI :(BOOL)slowTimeAlert andRestaurantID:(NSInteger)restaurand_id
{
//    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[getRestaurantListArray objectAtIndex:restaurand_id] valueForKey:@"id"]] forKey:@"restaurant_id"];
    [Dict setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
    [Dict setValue:[NSNumber numberWithBool:slowTimeAlert] forKey:@"is_show_time_alert"];
    
    NSMutableDictionary *mainDict = [NSMutableDictionary new];
    [mainDict setObject:Dict forKey:@"userslowtimealert"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/update_slow_time_alerts/",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            
            tmpArray = [NSMutableArray new];
            tmpArray = [getRestaurantListArray objectAtIndex:restaurand_id];
            
            NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc]init];
            tmpDict = [tmpArray mutableCopy];
            
            [tmpDict setObject: [NSNumber numberWithBool:slowTimeAlert] forKey:@"slowmode"];
          
            [getRestaurantListArray replaceObjectAtIndex:restaurand_id withObject:tmpDict];
            
            
            [tmpRestaurantArray replaceObjectAtIndex:restaurand_id withObject:tmpDict];
            
            //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
            
            //                [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
            //                DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
            
        }
        else
        {
            DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
        }
    }];
    
}

#pragma mark - SearchBar methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length == 0)
    {
        isFiltered = FALSE;
        searchBar.showsCancelButton = NO;
        isSearchClicked = NO;
        pageOffset = 1;

    }
    else
    {
        //        [filteredTableData removeAllObjects];
        
        isFiltered = true;
        searchBar.showsCancelButton = YES;
        isSearchClicked = YES;

        
        //        filteredTableData = [[NSMutableArray alloc] init];
        //
        //        int i=0;
        //        for (NSString *str in [poolTypesArray valueForKey:@"pooltype"])
        //        {
        //            NSRange nameRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
        //            NSRange descriptionRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
        //            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
        //            {
        //
        //                [filteredTableData addObject:[poolTypesArray objectAtIndex:i]];
        //            }
        //            i++;
        //        }
        
        [self searchAPI];
    }
    
    //    NSLog(@"filteredTableData == %@",filteredTableData);
    
    //    [tblView reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
//    self.viewTopConstraint.constant = 0.0f;
    [self.searchBar resignFirstResponder];
    self.searchBar.showsCancelButton = NO;
    self.searchBar.text = @"";
    isSearchClicked = NO;
    pageOffset  = 1;

    getRestaurantListArray = [tmpRestaurantArray mutableCopy];
    [self.tblView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.showsCancelButton = NO;
    isSearchClicked = YES;
//    pageOffset  = 1;
//    
//    [getRestaurantListArray removeAllObjects];
//    getRestaurantListArray = [[NSMutableArray alloc] init];
    
    [self searchAPI];
    [self.searchBar resignFirstResponder];
}

#pragma mark - SearchAPI

-(void)searchAPI
{
    // Added on 29-3
    pageOffset  = 1;
    [getRestaurantListArray removeAllObjects];
    getRestaurantListArray = [[NSMutableArray alloc] init];
    [self.tblView reloadData];   // till here

    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
   
    [Dict setValue:[NSString stringWithFormat:@"%d",pageOffset] forKey:@"page"];
    [Dict setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
    [Dict setValue:self.searchBar.text forKey:@"query"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/search_slow_time_alerts_venues/",BASEURL] withParam:Dict withCompletion:^(NSDictionary *response, BOOL success){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            
            //            venuesArray = [response valueForKey:@"venues"];
            [getRestaurantListArray addObjectsFromArray:[response valueForKey:@"venues"]];
            
            //            DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
            
            NSLog(@"venues_count == %d",[[response valueForKey:@"venues_count"] intValue]);
            
            venuesCount = [[response valueForKey:@"venues_count"] intValue];

            if ([[response valueForKey:@"venues_count"] intValue]%5 >= 0) {
                isMorePage=YES;
            }
            else
            {
                isMorePage=NO;
            }
            
            [self.tblView reloadData];
            isPageRefresing=NO;
            
            if (getRestaurantListArray.count == 0) {
                DisplayAlertControllerWithTitle(@"No data found", @"Q'd");
            }
        }
        else
        {
            
        }
    }];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  SettingsVC.h
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : NavigationClassVC<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    int pageOffset;
    BOOL isPageRefresing;
    BOOL isMorePage;
    BOOL isSearchClicked;

}
@end

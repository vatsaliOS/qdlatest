//
//  DetailBistroCell.h
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailBistroCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dayLbl;
@property (weak, nonatomic) IBOutlet UILabel *breakfastLbl;
@property (weak, nonatomic) IBOutlet UILabel *lunchLbl;
@property (weak, nonatomic) IBOutlet UILabel *dinnerLbl;


@end

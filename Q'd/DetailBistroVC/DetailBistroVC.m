//
//  DetailBistroVC.m
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "DetailBistroVC.h"
#import "DetailBistroCell.h"
#import "WaitingTimeVC.h"
#import <MapKit/MapKit.h>
#import <UIViewController+MMDrawerController.h>
#import "MBProgressHUD.h"

@interface DetailBistroVC ()
{
    IBOutlet UIButton *joinQueueBtn;
    __weak IBOutlet UILabel *titleLbl;
    __weak IBOutlet MKMapView *mapView;
    __weak IBOutlet UILabel *avgWaitingTime;
    __weak IBOutlet UILabel *addressLbl;
    __weak IBOutlet UILabel *restaurantNameLbl;
    __weak IBOutlet UIImageView *restaurantImgView;
    __weak IBOutlet UITextView *addressTxtView;
    __weak IBOutlet UIButton *slowTimeAlertBtn;
    __weak IBOutlet UILabel *alertMeLbl;
    
    UIButton *btnRight;
}
@end

@implementation DetailBistroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];    // Added on 27-1-2016
    
    [joinQueueBtn.layer setCornerRadius:5.0f];
    
    // commented on 11-3
    
    // 25 - Jan
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.bounds = CGRectMake( 0, 20, 85, 40 );
    [btnLeft addTarget:self action:@selector(backBtnClk:) forControlEvents:UIControlEventTouchUpInside];
    [btnLeft setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        btnLeft.titleLabel.font = [UIFont fontWithName:@"HindVadodara-Light" size:15.0f];
    }
    else if (IS_IPHONE_6 || IS_IPHONE_6P)
    {
        btnLeft.titleLabel.font = [UIFont fontWithName:@"HindVadodara-Light" size:18.0f];
    }

    [btnLeft setTitle:@"BACK" forState:UIControlStateNormal];
    [btnLeft setTitleEdgeInsets:UIEdgeInsetsMake(-2, -35, 0, 0)];
    [btnLeft setImageEdgeInsets:UIEdgeInsetsMake(0, -45, 0, 0)];
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
    self.navigationItem.leftBarButtonItem = backButton1;
    
    // till here on 11-3
    

//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];       // Added on 11-3


    CGRect frame = CGRectMake(87, 0, self.view.frame.size.width-187, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HindVadodara-Bold" size:18.0f];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = [NSString stringWithFormat:@"%@",[[self.restaurantDetailDict valueForKey:@"name"] uppercaseString]];
    self.navigationItem.titleView = label;
    
    btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnRight.bounds = CGRectMake(0, 23, 90, 30);

    btnRight.layer.cornerRadius = 5.0f;
    [btnRight.titleLabel setFont:[UIFont fontWithName:@"HindVadodara-BOLD" size:14.0f]];
    [btnRight.titleLabel setTextAlignment:NSTextAlignmentRight];
    [btnRight setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRight setBackgroundColor:RGBCOLOR(246, 210, 79)];
    [btnRight addTarget:self action:@selector(joinQueueBtnClk:) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        btnRight.bounds = CGRectMake(0, 20, 75, 25);
        
        alertMeLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:12.0];
        btnRight.titleLabel.font = [UIFont fontWithName:@"HindVadodara-BOLD" size:12.0];

    }
    else if (IS_IPHONE_6 || IS_IPHONE_6P)
    {
        btnRight.bounds = CGRectMake(0, 23, 90, 30);
        alertMeLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:16.0];
        btnRight.titleLabel.font = [UIFont fontWithName:@"HindVadodara-BOLD" size:14.0];
    }
    
//    [btnRight setImage:[UIImage imageNamed:@"join_btn@3x.png"] forState:UIControlStateNormal];
//    [btnRight setBackgroundImage:[UIImage imageNamed:@"joinQueueBtn"] forState:UIControlStateNormal];
//    
////    [btnRight setImageEdgeInsets:UIEdgeInsetsMake(0, 35, 0, 0)];
//    
////    btnRight.layer.cornerRadius = 5.0f;
//    
//    [btnRight addTarget:self action:@selector(joinQueueBtnClk:) forControlEvents:UIControlEventTouchUpInside];


    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
    self.navigationItem.rightBarButtonItem = backButton;
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [negativeSpacer setWidth:-10];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer,backButton,nil];
    
    self.navigationController.navigationBar.barTintColor = RGBCOLOR(255, 0, 78);
    self.navigationController.navigationBar.translucent = NO;
    
    [self loadUI];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeJoinQueueBtnTitle:) name:@"changeJoinQueueBtnTitle" object:nil];
    
//    [self.navigationController.navigationBar
//    setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    // till here

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    NSLog(@"self.restaurantDict === %@",self.restaurantDetailDict);

}

- (void)viewDidLayoutSubviews {
    [addressTxtView setContentOffset:CGPointZero animated:NO];
    addressTxtView.textContainer.lineFragmentPadding = 0;
    
    CGPoint scrollPoint = addressTxtView.contentOffset;
    scrollPoint.y= scrollPoint.y+13;
    [addressTxtView setContentOffset:scrollPoint animated:NO];
    
    [btnRight setSelected:NO];
    
    [addressTxtView setSelectable:NO];
    
     // commented on 29-3
    if (SharedObj.hasJoinedQueue) {
        [btnRight setTitle:@"In Queue" forState:UIControlStateNormal];
    }
    else
    {
        if ([[self.restaurantDetailDict valueForKey:@"after_seated"] intValue] == 1)
        {
            [btnRight setTitle:@"SEATED" forState:UIControlStateNormal];
            [btnRight setSelected:YES];
        }
        else
        {
            [btnRight setTitle:@"JOIN QUEUE" forState:UIControlStateNormal];
        }
    }
    
    if (!SharedObj.isSlowModeOn) {
        [slowTimeAlertBtn setBackgroundImage:[UIImage imageNamed:@"OffBtn"] forState:UIControlStateNormal];
        slowTimeAlertBtn.selected = NO;
    }
    else
    {
        [slowTimeAlertBtn setBackgroundImage:[UIImage imageNamed:@"OnBtn"] forState:UIControlStateNormal];
        slowTimeAlertBtn.selected = YES;
    }
    
    [self.view bringSubviewToFront:slowTimeAlertBtn];

}

-(void)changeJoinQueueBtnTitle:(NSNotification*)notification
{
    NSLog(@"notofication == %@",notification);
    
}

#pragma mark - LoadUI
-(void)loadUI
{
//    titleLbl.text = [[self.restaurantDetailDict valueForKey:@"name"] uppercaseString];
    avgWaitingTime.text = [NSString stringWithFormat:@"%@ min average wait",[self.restaurantDetailDict valueForKey:@"average_waiting_time"]];
    
    NSString *fontname = @"HindVadodara-Light";
    if (IS_IPHONE_4_OR_LESS) {
        avgWaitingTime.font = [UIFont fontWithName:fontname size:12.0f];
        [addressTxtView setFont:[UIFont fontWithName:@"HindVadodara-Regular" size:12.0]];

    }
    else if (IS_IPHONE_5){
        avgWaitingTime.font = [UIFont fontWithName:fontname size:13.0f];
        [addressTxtView setFont:[UIFont fontWithName:@"HindVadodara-Regular" size:13.0]];
    }
    else
    {
        avgWaitingTime.font = [UIFont fontWithName:fontname size:15.0f];
        [addressTxtView setFont:[UIFont fontWithName:@"HindVadodara-Regular" size:15.0]];
    }

    
    [restaurantImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.restaurantDetailDict valueForKey:@"image"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
//    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
//    [locationManager requestWhenInUseAuthorization];
//    [locationManager requestAlwaysAuthorization];
//    
//    locationManager.delegate = self;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    locationManager.distanceFilter = kCLDistanceFilterNone;
//    [locationManager startUpdatingLocation];

    MKCoordinateRegion region = mapView.region;
    
    double latitude = [[self.restaurantDetailDict valueForKey:@"latitude"] doubleValue];
    double longitude = [[self.restaurantDetailDict valueForKey:@"longitude"] doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
//    CLLocation *location = [locationManager location];
//    coordinate.longitude = location.coordinate.longitude;
//    coordinate.latitude = location.coordinate.latitude;
    
    region.center = CLLocationCoordinate2DMake(latitude, longitude);
    region.span.longitudeDelta /= 3000.0; // Bigger the value, closer the map view
    region.span.latitudeDelta /= 3000.0;
    
    [mapView setRegion:region animated:YES]; // Choose if you want animate or not
    
    MKPointAnnotation *mapPin = [[MKPointAnnotation alloc] init];

    mapPin.title = [NSString stringWithFormat:@"%@",[self.restaurantDetailDict valueForKey:@"name"]];
    mapPin.coordinate = coordinate;
    
    [mapView addAnnotation:mapPin];
    
    // Added on 28-3
    
    CGPoint fakecenter = CGPointMake(300, 200);
    CLLocationCoordinate2D tmpCoordinate = [mapView convertPoint:fakecenter toCoordinateFromView:mapView];
    [mapView setCenterCoordinate:tmpCoordinate animated:YES];
    
    // till here

    restaurantNameLbl.text = [NSString stringWithFormat:@"%@",[self.restaurantDetailDict valueForKey:@"name"]];
    addressLbl.text = [self.restaurantDetailDict valueForKey:@"address"];
    
    addressTxtView.text = [self.restaurantDetailDict valueForKey:@"address"];
    
//    addressTxtView.textContainerInset = UIEdgeInsetsZero;
//    addressTxtView.textContainer.lineFragmentPadding = 0;
//    
//    self.automaticallyAdjustsScrollViewInsets = NO;
    
    NSRange r  = {0,0};
    [addressTxtView setSelectedRange:r];
    
}

#pragma mark - MapViewDelegate

-(MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *reuseId = @"currentloc";
    
    MKPinAnnotationView *annView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    
    if (annotation == mapView.userLocation) {
        return nil;
    }
    if (annView == nil)
    {
        annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        annView.animatesDrop = NO;
        annView.canShowCallout = YES;
        annView.calloutOffset = CGPointMake(-5, 5);
    }
    else
    {
        annView.annotation = annotation;
    }
    
    annView.pinColor = MKPinAnnotationColorPurple;
    return annView;
}

#pragma mark - TableView Methods

- (UIView*) tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(100, 202, tableView.bounds.size.width, 200)];
    UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectMake(12,8,200,28)];
//    [customLabel setFont:[UIFont systemFontOfSize:22]];
    [customLabel setFont:[UIFont fontWithName:@"HindVadodara-Light" size:22.0f]];
    [customLabel setTextColor:RGBCOLOR(108, 108, 108)];
    customLabel.text = @"Trading hours";
    [headerView setBackgroundColor:[UIColor whiteColor]];
    [headerView addSubview:customLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.restaurantDetailDict valueForKey:@"opening_hours"] count]+1;
}                                                        

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailBistroCell *cell = (DetailBistroCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.backgroundColor = [UIColor whiteColor];
    //    cell.cellImgView.image = [UIImage imageNamed:@""];
    
    NSString *fontname = @"";
    if (indexPath.row == 0) {
        fontname = @"HindVadodara-Bold";
        
        cell.dayLbl.text = @"Day";
        cell.breakfastLbl.text = @"Breakfast";
        cell.lunchLbl.text = @"Lunch";
        cell.dinnerLbl.text = @"Dinner";
        
//        cell.dayLbl.font = [UIFont fontWithName:@"Hind-Bold" size:11.0f];
//        cell.startTimeLbl.font = [UIFont fontWithName:@"Hind-Bold" size:11.0f];
//        cell.endTimeLbl.font = [UIFont fontWithName:@"Hind-Bold" size:11.0f];
        
        if (IS_IPHONE_4_OR_LESS) {
            cell.dayLbl.font = [UIFont fontWithName:fontname size:10.0f];
            cell.breakfastLbl.font = [UIFont fontWithName:fontname size:10.0f];
            cell.lunchLbl.font = [UIFont fontWithName:fontname size:10.0f];
            cell.dinnerLbl.font = [UIFont fontWithName:fontname size:10.0f];
            
        }
        else if (IS_IPHONE_5){
            cell.dayLbl.font = [UIFont fontWithName:fontname size:10.0f];
            cell.breakfastLbl.font = [UIFont fontWithName:fontname size:10.0f];
            cell.lunchLbl.font = [UIFont fontWithName:fontname size:10.0f];
            cell.dinnerLbl.font = [UIFont fontWithName:fontname size:10.0f];
        }
        else if (IS_IPHONE_6)
        {
            cell.dayLbl.font = [UIFont fontWithName:fontname size:10.0f];
            cell.breakfastLbl.font = [UIFont fontWithName:fontname size:10.0f];
            cell.lunchLbl.font = [UIFont fontWithName:fontname size:10.0f];
            cell.dinnerLbl.font = [UIFont fontWithName:fontname size:10.0f];
        }
        else
        {
            cell.dayLbl.font = [UIFont fontWithName:fontname size:13.0f];
            cell.breakfastLbl.font = [UIFont fontWithName:fontname size:13.0f];
            cell.lunchLbl.font = [UIFont fontWithName:fontname size:13.0f];
            cell.dinnerLbl.font = [UIFont fontWithName:fontname size:13.0f];
        }

    }
    else
    {
        fontname = @"HindVadodara-Regular";
        
        NSString *breakfastStartTime,*breakfastEndTime,*lunchStartTime,*lunchEndTime,*dinnerStartTime,*dinnerEndTime;
        
        breakfastStartTime = [[[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row-1] valueForKey:@"Breakfast"]valueForKey:@"start"];
        breakfastEndTime = [[[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row-1] valueForKey:@"Breakfast"]valueForKey:@"end"];
        
        lunchStartTime = [[[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row-1] valueForKey:@"Lunch"]valueForKey:@"start"];
        lunchEndTime = [[[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row-1] valueForKey:@"Lunch"]valueForKey:@"end"];
        
        dinnerStartTime = [[[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row-1] valueForKey:@"Dinner"]valueForKey:@"start"];
        dinnerEndTime = [[[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row-1] valueForKey:@"Dinner"]valueForKey:@"end"];
        
        if ([breakfastStartTime containsString:@":00 "]) {
           breakfastStartTime = [breakfastStartTime stringByReplacingOccurrencesOfString:@":00 " withString:@""];
           breakfastStartTime = [breakfastStartTime stringByReplacingOccurrencesOfString:@" " withString:@""];

        }
        if ([breakfastEndTime containsString:@":00 "]) {
           breakfastEndTime = [breakfastEndTime stringByReplacingOccurrencesOfString:@":00 " withString:@""];
           breakfastEndTime = [breakfastEndTime stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        if ([lunchStartTime containsString:@":00 "]) {
           lunchStartTime = [lunchStartTime stringByReplacingOccurrencesOfString:@":00 " withString:@""];
            lunchStartTime = [lunchStartTime stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        if ([lunchEndTime containsString:@":00 "]) {
           lunchEndTime = [lunchEndTime stringByReplacingOccurrencesOfString:@":00 " withString:@""];
            lunchEndTime = [lunchEndTime stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        if ([dinnerStartTime containsString:@":00 "]) {
           dinnerStartTime = [dinnerStartTime stringByReplacingOccurrencesOfString:@":00 " withString:@""];
            dinnerStartTime = [dinnerStartTime stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        if ([dinnerEndTime containsString:@":00 "]) {
           dinnerEndTime = [dinnerEndTime stringByReplacingOccurrencesOfString:@":00 " withString:@""];
            dinnerEndTime = [dinnerEndTime stringByReplacingOccurrencesOfString:@" " withString:@""];
        }

        cell.dayLbl.text = [[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row-1]valueForKey:@"day"];
        
        if ([breakfastStartTime isEqualToString:@"N/A"] && [breakfastEndTime isEqualToString:@"N/A"]) {
            cell.breakfastLbl.text = [NSString stringWithFormat:@"N/A"];
        }
        else{
            cell.breakfastLbl.text = [NSString stringWithFormat:@"%@-%@",breakfastStartTime,breakfastEndTime];
        }
        
        if ([lunchStartTime isEqualToString:@"N/A"] && [lunchEndTime isEqualToString:@"N/A"]) {
            cell.lunchLbl.text = [NSString stringWithFormat:@"N/A"];
        }
        else
        {
            cell.lunchLbl.text = [NSString stringWithFormat:@"%@-%@",lunchStartTime,lunchEndTime];
        }
        
        if ([dinnerStartTime isEqualToString:@"N/A"] && [dinnerEndTime isEqualToString:@"N/A"]) {
            cell.dinnerLbl.text = [NSString stringWithFormat:@"N/A"];
        }
        else
        {
            cell.dinnerLbl.text = [NSString stringWithFormat:@"%@-%@",dinnerStartTime,dinnerEndTime];
        }

//        cell.dayLbl.font = [UIFont fontWithName:@"Hind-Regular" size:11.0f];
//        cell.startTimeLbl.font = [UIFont fontWithName:@"Hind-Regular" size:11.0f];
//        cell.endTimeLbl.font = [UIFont fontWithName:@"Hind-Regular" size:11.0f];
        
        if (IS_IPHONE_4_OR_LESS) {
            cell.dayLbl.font = [UIFont fontWithName:fontname size:8.0f];
            cell.breakfastLbl.font = [UIFont fontWithName:fontname size:8.0f];
            cell.lunchLbl.font = [UIFont fontWithName:fontname size:8.0f];
            cell.dinnerLbl.font = [UIFont fontWithName:fontname size:8.0f];
            
        }
        else if (IS_IPHONE_5){
            cell.dayLbl.font = [UIFont fontWithName:fontname size:8.0f];
            cell.breakfastLbl.font = [UIFont fontWithName:fontname size:8.0f];
            cell.lunchLbl.font = [UIFont fontWithName:fontname size:8.0f];
            cell.dinnerLbl.font = [UIFont fontWithName:fontname size:8.0f];
        }
        else
        {
            cell.dayLbl.font = [UIFont fontWithName:fontname size:11.0f];
            cell.breakfastLbl.font = [UIFont fontWithName:fontname size:11.0f];
            cell.lunchLbl.font = [UIFont fontWithName:fontname size:11.0f];
            cell.dinnerLbl.font = [UIFont fontWithName:fontname size:11.0f];
        }

    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark BackBtn Click
-(IBAction)backBtnClk:(id)sender
{
//    [[appDelegate locationManager] startUpdatingLocation];    // commented on 11-2

    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma JoinQueue Btn Click
- (IBAction)joinQueueBtnClk:(id)sender
{
    
    if (![btnRight isSelected]) {
        [btnRight setSelected:YES];
    
        if (!SharedObj.hasJoinedQueue) {        // When user is Joining Queue for first time
            
            __block NSArray *pwdArray;
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Q'd" message:@"Enter number of people who are coming" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                NSLog(@"Cancel action");
                [btnRight setSelected:NO];
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                NSLog(@"Done action");
                
                pwdArray = alert.textFields;
                NSLog(@"OldStr == %@",[[pwdArray objectAtIndex:0] valueForKey:@"text"]);
                
                NSMutableDictionary *Dict = [NSMutableDictionary new];
                [Dict setValue:[NSString stringWithFormat:@"%@",[self.restaurantDetailDict valueForKey:@"id"]] forKey:@"restaurant_id"];
                [Dict setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
                [Dict setValue:[NSString stringWithFormat:@"%@",[[pwdArray objectAtIndex:0] valueForKey:@"text"]] forKey:@"number_of_people"];
                
                NSMutableDictionary *mainDict = [NSMutableDictionary new];
                [mainDict setObject:Dict forKey:@"joinqueue"];
                
                if ([[[pwdArray objectAtIndex:0] valueForKey:@"text"] isEqualToString:@""] || [[[pwdArray objectAtIndex:0] valueForKey:@"text"] length] > 2)
                {
                    [btnRight setSelected:NO];
                    DisplayAlertControllerWithTitle(@"Enter valid number of people", @"Q'd");
                }
                else
                {
                    [self joinQueueAPI : mainDict];
                }
            }]];
            
            [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                textField.placeholder = @"Number of people";
                textField.secureTextEntry = NO;
                textField.delegate = self;
                [textField setKeyboardType:UIKeyboardTypePhonePad];
                
            }];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            NSMutableDictionary *Dict = [NSMutableDictionary new];
            [Dict setValue:[NSString stringWithFormat:@"%@",[self.restaurantDetailDict valueForKey:@"id"]] forKey:@"restaurant_id"];
            [Dict setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
            
            NSMutableDictionary *mainDict = [NSMutableDictionary new];
            [mainDict setObject:Dict forKey:@"joinqueue"];
            
//            [self joinQueueAPI : mainDict];   // commented on 10-3
            
            [self performSegueWithIdentifier:@"WaitingTimeVC" sender:mainDict]; // Added on 10-3
        }

    }
    
}

#pragma mark - JoinQueueAPI
-(void)joinQueueAPI : (NSMutableDictionary*)Dict
{
    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/join_queue/",BASEURL] withParam:Dict withCompletion:^(NSDictionary *response, BOOL success){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
            
//            SharedObj.hasJoinedQueue = true;    // commented on 10-3

            [self performSegueWithIdentifier:@"WaitingTimeVC" sender:response];
            
//            if ([[self.restaurantDetailDict valueForKey:@"is_joined"] isEqualToNumber:[NSNumber numberWithInt:0]]) {
//                [self.restaurantDetailDict setValue:[NSNumber numberWithInt:1] forKey:@"is_joined"];
//            }
            
        }
        else
        {
            DisplayAlertControllerWithTitle(@"Something went wrong please try again", @"Q'd");
        }
    }];

}

#pragma mark - SlowTimeAlert Click
-(IBAction)slowAlertBtnClk:(id)sender
{
    BOOL isSlowTimeAlert;
    if (![slowTimeAlertBtn isSelected]) {
        [slowTimeAlertBtn setBackgroundImage:[UIImage imageNamed:@"OnBtn"] forState:UIControlStateNormal];
        isSlowTimeAlert = TRUE;
        
        SharedObj.isSlowModeOn = TRUE;

        [slowTimeAlertBtn setSelected:YES];
    }
    else
    {
        [slowTimeAlertBtn setBackgroundImage:[UIImage imageNamed:@"OffBtn"] forState:UIControlStateNormal];
        isSlowTimeAlert = FALSE;
        
        [slowTimeAlertBtn setSelected:NO];
        SharedObj.isSlowModeOn = FALSE;
    }
    
    [self updateSlowTimeAlertAPI:isSlowTimeAlert];
}

-(void)updateSlowTimeAlertAPI :(BOOL)slowTimeAlert
{
//    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%@",[self.restaurantDetailDict valueForKey:@"id"]] forKey:@"restaurant_id"];
    [Dict setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
    [Dict setValue:[NSNumber numberWithBool:slowTimeAlert] forKey:@"is_show_time_alert"];
    
    NSMutableDictionary *mainDict = [NSMutableDictionary new];
    [mainDict setObject:Dict forKey:@"userslowtimealert"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/update_slow_time_alerts/",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
        }
        else
        {
            DisplayAlertControllerWithTitle(@"Something went wrong please try again", @"Q'd");
        }
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    WaitingTimeVC *VC = (WaitingTimeVC*)[segue destinationViewController];
    
    VC.restaurantIDStr = [self.restaurantDetailDict valueForKey:@"id"];
    
    if (!SharedObj.hasJoinedQueue) {
    
        // Added on 15-3
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[self.restaurantDetailDict valueForKey:@"id"]] forKey:@"restaurant_id"];
        [Dict setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"joinqueue"];
        
        VC.parameterDict = mainDict;
        // till here
        
        VC.detailInformationDict = [sender valueForKey:@"queue_details"];
    }
    else
    {
        VC.parameterDict = sender;
    }
    
}


@end

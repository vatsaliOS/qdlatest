//
//  ViewController.m
//  Q'd
//
//  Created by SOTSYS028 on 11/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "ViewController.h"
#import "SignUpVC.h"
#import "DrawerVC.h"
#import "Common.h"
#import "MBProgressHUD.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *pwdView;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn;
@property (weak, nonatomic) IBOutlet UIButton *createAccount;
@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxtField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard1)];
    [self.view addGestureRecognizer:tap];
    [self changeUI];
     [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)dismissKeyboard1 {
    //    [transparentViewAboveTbl setHidden:YES];
    
//    if ([self.emailTxtField respondsToSelector:@selector(becomeFirstResponder)]) {
        [self.emailTxtField resignFirstResponder];
//    }
//    else if ([self.pwdTxtField respondsToSelector:@selector(becomeFirstResponder)]) {
        [self.pwdTxtField resignFirstResponder];
//    }
}

-(void)viewWillAppear:(BOOL)animated
{
    self.pwdTxtField.text = @"";
    self.emailTxtField.text = @"";

    [self.navigationController setNavigationBarHidden:YES animated:YES];  // Use this when using custom Navigation bar
    
    if (!SharedObj.hasLogin) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Q'd" message:@"User logout successfully" preferredStyle:UIAlertControllerStyleAlert]; [self presentViewController:alert animated:YES completion:nil];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
            
        });
   
    }
    
//    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - UI Design using Code

-(void)changeUI
{
    // e,ail View layer
    [self.emailView.layer setCornerRadius:5.0f];
    [self.emailView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.emailView.layer setBorderWidth:1.0f];
    
    // Pwd View layer
    [self.pwdView.layer setCornerRadius:5.0f];
    [self.pwdView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.pwdView.layer setBorderWidth:1.0f];
    
    // SignIn button
    [self.signInBtn.layer setCornerRadius:5.0f];
    [self.signInBtn.layer setBorderColor:[UIColor clearColor].CGColor];
    [self.signInBtn.layer setBorderWidth:2.0f];
    
    // Create Account
    [self.createAccount.layer setCornerRadius:5.0f];
    [self.createAccount.layer setBorderColor:[UIColor clearColor].CGColor];
    [self.createAccount.layer setBorderWidth:2.0f];
}

#pragma MARK - IBAction Methods

- (IBAction)signInBtnClk:(id)sender {
   
    [self.emailTxtField resignFirstResponder];
    [self.pwdTxtField resignFirstResponder];
    
    if ([self.emailTxtField.text isEqualToString:@""]) {
        DisplayAlertControllerWithTitle(@"Email address can't be blank", @"Q'd");
        
        return;
    }
    else if ([self.pwdTxtField.text isEqualToString:@""])
    {
        DisplayAlertControllerWithTitle(@"Password can't be blank", @"Q'd");
        return;
    }
    
    BOOL isValidEmail = [Common validateEmail:self.emailTxtField.text];
    
    if (!isValidEmail){
        DisplayAlertControllerWithTitle(@"Enter your valid email address", @"Q'd");
    }
    else
    {
        MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:self.emailTxtField.text forKey:@"email"];
        [Dict setValue:self.pwdTxtField.text forKey:@"password"];
        [Dict setValue:@"1" forKey:@"device_type"];
        NSString *tokenValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"DeviceToken"];
        [Dict setValue:tokenValue forKey:@"device_token"];
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"user"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/auth/signin",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (success)
            {
                NSLog(@"response in success = %@",response);
                //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
                [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"user"] forKey:@"UserDetails"];
                
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"HasLogin"];
//                [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
//                DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                
                [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
                
            }
            else
            {
                DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
            }
        }];

    }
    
}

- (IBAction)createAccountBtnClk:(id)sender {
    
//    [self performSegueWithIdentifier:@"SignUpVC" sender:nil];
    
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"signUpVC"] animated:YES];
    
}

#pragma mark TextField Delegates
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.emailTxtField) {
        
        BOOL isValidEmail = [Common validateEmail:self.emailTxtField.text];
        
        if (isValidEmail){
            
        }
        else{
            
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.emailTxtField) {
        [self.pwdTxtField becomeFirstResponder];
    }
    else
    {
        [self.pwdTxtField resignFirstResponder];
    }
    return YES;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
//    if ([segue.identifier isEqualToString:@"SignUpVC"]) {
//        SignUpVC *VC = (SignUpVC*)[segue destinationViewController];
//    }
    if ([segue.identifier isEqualToString:@"DrawerVC"]) {
        DrawerVC *VC = (DrawerVC*)[segue destinationViewController];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

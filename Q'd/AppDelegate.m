//
//  AppDelegate.m
//  Q'd
//
//  Created by SOTSYS028 on 11/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "AppDelegate.h"
#import "DrawerVC.h"
#import "Common.h"
#import <IQKeyboardManager.h>
#import "ListViewVC/ListViewVC.h"
#import "WaitingTimeVC/WaitingTimeVC.h"



@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    self.locationManager = [[CLLocationManager alloc]init];
    
    [[self locationManager] requestWhenInUseAuthorization];
//    [[self locationManager] requestAlwaysAuthorization];
    
    [self locationManager].delegate = self;
    [self locationManager].desiredAccuracy = kCLLocationAccuracyBest;
    [self locationManager].distanceFilter = kFixedDistanceFilter;

    [[self locationManager] startUpdatingLocation];
    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
//    {
        //ios8 ++
    
        [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        }
        else {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }

    SharedObj.hasLogin = YES;
    
//    }
//    else
//    {
//        // ios7
//        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotificationTypes:)])
//        {
//            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
//        }
//    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];        // Added on 23-Feb
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"HasLogin"])
    {
        
//        NSDictionary *remoteNotifiInfo = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
//        if (remoteNotifiInfo) {
//            NSDictionary *dictTemp =[remoteNotifiInfo valueForKey:@"aps"];
//            
//        }
//        else{
//            
//        }
        
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        DrawerVC *VC = (DrawerVC*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DrawerVC"];
        
        UINavigationController *nav = [[UINavigationController alloc]init];
        [nav pushViewController:VC animated:YES];
        [nav setNavigationBarHidden:YES];
        self.window.rootViewController = nav;
        [self.window makeKeyAndVisible];
    }
    self.window.backgroundColor = [UIColor whiteColor];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//    [self.locationManager startUpdatingLocation];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"methodCalled" object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    [[NSNotificationCenter defaultCenter] postNotificationName:@"applicationDidBecomeActiveCall" object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark PushNotification methods

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings // available in iOS8
{
    [application registerForRemoteNotifications];
}
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
//    XSET_VALUE(token, @"DeviceToken");
    
    [[NSUserDefaults standardUserDefaults]setValue:token forKey:@"DeviceToken"];
    NSLog(@"Device Token = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"DeviceToken"]);
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // Handle your remote RemoteNotification
    
    UIApplicationState state = [application applicationState];
    
    NSLog(@"PUSH userInfo == %@",userInfo);
    
//    DisplayAlertWithTitle(userInfo, @"Q'd Test");
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"changeJoinQueueBtnTitle" object:userInfo];     // Added on 29-3
    
    if(state == UIApplicationStateInactive)
    {
        
    }
    else if (state==UIApplicationStateActive)
    {
        NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        DisplayAlertWithTitle(message, @"Q'd");
    }
    else if (state ==UIApplicationStateBackground)
    {

    }
    
   

}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Error:%@",error);
}

@end

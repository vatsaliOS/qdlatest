//
//  SignUpVC.m
//  Q'd
//
//  Created by SOTSYS028 on 11/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "SignUpVC.h"
#import "confirmationCodeVC.h"
#import "Common.h"
#import "MBProgressHUD.h"
#import "DrawerVC.h"

@interface SignUpVC ()
{
    CLLocationManager *locationManager;
    float Lat,Long;
    
    BOOL isFirstTime;
    __weak IBOutlet UITableView *tblView;
    NSMutableArray *placeholderArray;
    NSMutableArray *requestArray;
    NSMutableArray *txtFieldArray;
}
@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@property (weak, nonatomic) IBOutlet UITextField *phnNoTxtField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxtField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPwdTxtField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxtField;

@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isFirstTime = YES;
    
    requestArray = [[NSMutableArray alloc]init];
    
    txtFieldArray = [[NSMutableArray alloc]init];
    
    for (int i =0; i<7; i++) {
        [requestArray addObject:@""];
    }
    
    placeholderArray = [[NSMutableArray alloc]initWithObjects:@"First Name", @"Last Name", @"Email",@"Phone number", @"Password", @"Confirm password", nil];

    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundImage"]]]; //topSignUp

    [self showNavigationbar];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



-(void)viewWillAppear:(BOOL)animated
{
       // Use this when using custom Navigation bar
    [self setLeftBtnWithImageName:@"Back" andLSelector:@selector(backBtnClk:)];   // Use this when using custom Navigation bar
        self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;

    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
    
    CLLocation *location = [locationManager location];
    // Configure the new event with information from the location
    CLLocationCoordinate2D coordinate = [location coordinate];
    Lat = coordinate.latitude;
    Long = coordinate.longitude;
    
//    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
//    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
//    NSLog(@"dLatitude : %@", latitude);
//    NSLog(@"dLongitude : %@",longitude);
    
//    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[UINavigationBar appearance] setBarTintColor:RGBCOLOR(255, 0, 78)];
//    self.navigationController.navigationBar.barTintColor = RGBCOLOR(243, 67, 75);
}

-(IBAction)backBtnClk:(id)sender
{
    SharedObj.hasLogin = YES;
    
    [self.navigationController popViewControllerAnimated:YES];
//      [self hideNavigationbar];
}

-(IBAction)createAccountBtnClk:(id)sender
{
    
    BOOL checkEmptyTxt = NO;
    for (int i = 0; i<6; i++) {
       
        
        
//        for (UIView *view in  cell.contentView.subviews){
//            
//            if ([view isKindOfClass:[UITextField class]]){
//                
//                UITextField* txtField = (UITextField *)view;
//                
//                if (txtField.tag == indexPath.row) {
//                    NSLog(@"TextField.tag:%ld and Data %@", (long)txtField.tag, txtField.text);
//                }
//            } // End of Cell Sub View
//        }// Counter Loop
        
        UITextField *tmpTxtField = [txtFieldArray objectAtIndex:i];
        NSLog(@"tmpTxztField bvalue == %@",tmpTxtField.text);
        
        if ([tmpTxtField.text isEqualToString:@""]) {
            
            checkEmptyTxt = YES;
        }
        [tmpTxtField resignFirstResponder];
        [requestArray replaceObjectAtIndex:i withObject:tmpTxtField.text];

    }
    
    if (checkEmptyTxt) {
        DisplayAlertControllerWithTitle(@"All Fields are mandetory", @"Q'd");
        return;
    }
    else
    {
    
        BOOL isValidEmail = [Common validateEmail:[NSString stringWithFormat:@"%@",[requestArray objectAtIndex:2]]];
        
        if (!isValidEmail){
            DisplayAlertControllerWithTitle(@"Enter your valid email address", @"Q'd");
            return;
        }
        
        NSString *pwdStr = [NSString stringWithFormat:@"%@",[requestArray objectAtIndex:4]];
        NSString *confirmPwdStr = [NSString stringWithFormat:@"%@",[requestArray objectAtIndex:5]];
        
        NSUInteger pwdLen = [pwdStr length];
        NSUInteger confirmPwdLen = [confirmPwdStr length];
        
        if (pwdLen < 6 || confirmPwdLen < 6)
        {
            DisplayAlertControllerWithTitle(@"Password must contain minimum 6 characters", @"Q'd");
            return;
        }

        else if (![pwdStr isEqualToString:confirmPwdStr])
        {
            DisplayAlertControllerWithTitle(@"Password and Confirm Password not matched", @"Q'd");
        }
        else
        {
            isFirstTime = NO;
            
            MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Loading...";
            
            NSMutableDictionary *Dict = [NSMutableDictionary new];
            [Dict setValue:[requestArray objectAtIndex:0] forKey:@"first_name"];
            [Dict setValue:[requestArray objectAtIndex:1] forKey:@"last_name"];
            [Dict setValue:[requestArray objectAtIndex:2] forKey:@"email"];
            [Dict setValue:[requestArray objectAtIndex:4] forKey:@"password"];
            [Dict setValue:[requestArray objectAtIndex:3] forKey:@"phone"];
            [Dict setValue:[requestArray objectAtIndex:5] forKey:@"password_confirmation"];
            [Dict setValue:[NSString stringWithFormat:@"%f",Lat] forKey:@"latitude"];
            [Dict setValue:[NSString stringWithFormat:@"%f",Long] forKey:@"longitude"];
            [Dict setValue:@"1" forKey:@"device_type"];
            NSString *tokenValue = [[NSUserDefaults standardUserDefaults]valueForKey:@"DeviceToken"];
            [Dict setValue:tokenValue forKey:@"device_token"];
//            [Dict setValue:XGET_VALUE(@"DeviceToken") forKey:@"device_token"];
            
            NSMutableDictionary *mainDict = [NSMutableDictionary new];
            [mainDict setObject:Dict forKey:@"user"];
            
            [Common postServiceWithURL:[NSString stringWithFormat:@"%@/auth/signup",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (success)
                {
                    NSLog(@"response in success = %@",response);
                    [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"user"] forKey:@"UserDetails"];
                    
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"HasLogin"];
//                    [self performSegueWithIdentifier:@"DrawerVC" sender:nil];

//                    [self performSegueWithIdentifier:@"confirmationCodeVC" sender:nil];
//                    DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                    
                    requestArray = nil;
                    
                    [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
                   
                }
                else
                {
                    NSString *responseMessage = [response valueForKey:@"message"];
                    DisplayAlertControllerWithTitle(responseMessage, @"Q'd");
                }
            }];

        }
       
    }
}

#pragma mark TextField Delegates
-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    if (textField == self.emailTxtField) {
//        
//      BOOL isValidEmail = [Common validateEmail:self.emailTxtField.text];
//    
//        if (isValidEmail){
//            
//        }
//        else{
//            
//        }
//    }
    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
//    
////    if (textField.tag != ) {
//        txtFieldCell *cell = [tblView cellForRowAtIndexPath:indexPath];
//        [requestArray replaceObjectAtIndex:indexPath.row withObject:cell.txtField.text];
//    }
//    else {
//        txtFieldCell *cell = [tblView cellForRowAtIndexPath:indexPath];
//        [requestArray insertObject:cell.txtField.text atIndex:indexPath.row];
//    }

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    
//    if (textField == self.emailTxtField) {
//        
//        [self.phnNoTxtField becomeFirstResponder];
//    }
//    else if (textField == self.phnNoTxtField)
//    {
//        [self.pwdTxtField becomeFirstResponder];
//    }
//    else if (textField == self.pwdTxtField)
//    {
//        [self.confirmPwdTxtField becomeFirstResponder];
//    }
//    else if (textField == self.confirmPwdTxtField)
//    {
//        [textField resignFirstResponder];
//    }
//    else if (textField == self.firstNameTxtField)
//    {
//        [self.lastNameTxtField becomeFirstResponder];
//    }
//    else if (textField == self.lastNameTxtField)
//    {
//        [self.emailTxtField becomeFirstResponder];
//    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
    
    NSIndexPath *newPath = [NSIndexPath indexPathForRow:textField.tag+1 inSection:0];
    
    if (textField.tag != 6) {
        txtFieldCell *cell = [tblView cellForRowAtIndexPath:newPath];
        [cell.txtField becomeFirstResponder];
//        [requestArray insertObject:cell.txtField atIndex:textField.tag];
    }
    else {
        txtFieldCell *cell = [tblView cellForRowAtIndexPath:indexPath];
        [cell.txtField resignFirstResponder];
//        [requestArray insertObject:cell.txtField atIndex:textField.tag];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tableview Methods

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_4_OR_LESS) {
        if (indexPath.row == 0) {
            return 50;
        }
        else if (indexPath.row == 7)
        {
            return 60;
        }
        else
        {
            return 60;
        }
    }
    else if (IS_IPHONE_5) {
        if (indexPath.row == 0) {
            return 60;
        }
        else if (indexPath.row == 7)
        {
            return 75;
        }
        else
        {
            return 72;
        }
    }
    else if (IS_IPHONE_6 || IS_IPHONE_6P)
    {
        if (indexPath.row == 0) {
            return 70;
        }
        else if (indexPath.row == 7)
        {
            return 85;
        }
        else
        {
            return 82;
        }
    }
    else
    {
        return UITableViewAutomaticDimension;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_4_OR_LESS) {
        if (indexPath.row == 0) {
            return 55;
        }
        else if (indexPath.row == 7)
        {
            return 60;
        }
        else
        {
            return 60;
        }
    }
    else if (IS_IPHONE_5) {
        if (indexPath.row == 0) {
            return 70;
        }
        else if (indexPath.row == 7)
        {
            return 75;
        }
        else
        {
            return 72;
        }
    }
    else if (IS_IPHONE_6 || IS_IPHONE_6P)
    {
        if (indexPath.row == 0) {
            return 70;
        }
        else if (indexPath.row == 7)
        {
            return 85;
        }
        else
        {
            return 82;
        }
    }
    else
    {
        return UITableViewAutomaticDimension;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"lblCell"];
        
        UILabel *lbl = (UILabel*)[cell viewWithTag:99];
        if (IS_IPHONE_4_OR_LESS) {
            [lbl setFont:[UIFont fontWithName:@"HindVadodara-Regular" size:18.0f]];
        }
        else if (IS_IPHONE_5) {
            [lbl setFont:[UIFont fontWithName:@"HindVadodara-Regular" size:23.0f]];
        }
        else if (IS_IPHONE_6 || IS_IPHONE_6P)
        {
            [lbl setFont:[UIFont fontWithName:@"HindVadodara-Regular" size:26.0f]];
        }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == 7)
    {
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"btnCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }
    else
    {
        txtFieldCell *cell = (txtFieldCell*)[tableView dequeueReusableCellWithIdentifier:@"txtFieldCell"];
        cell.txtField.tag = indexPath.row;
        
        if (IS_IPHONE_4_OR_LESS) {
            [cell.txtField setFont:[UIFont fontWithName:@"HindVadodara-Regular" size:16.0f]];
        }
        else if (IS_IPHONE_5) {
            [cell.txtField setFont:[UIFont fontWithName:@"HindVadodara-Regular" size:20.0f]];
        }
        else if (IS_IPHONE_6 || IS_IPHONE_6P)
        {
            [cell.txtField setFont:[UIFont fontWithName:@"HindVadodara-Regular" size:23.0f]];
        }

        if (cell.txtField.tag == 3)
        {
            cell.txtField.keyboardType = UIKeyboardTypeEmailAddress;
        }
        else if (cell.txtField.tag == 4)
        {
            cell.txtField.keyboardType = UIKeyboardTypePhonePad;
        }
        else if (cell.txtField.tag == 5 || cell.txtField.tag == 6) {
            cell.txtField.secureTextEntry = YES;
            if (cell.txtField.tag == 6) {
                cell.txtField.returnKeyType = UIReturnKeyDone;
            }
        }
        
        if ([cell.txtField.text isEqualToString:@""]) {
             cell.txtField.placeholder = [NSString stringWithFormat:@"%@",[placeholderArray objectAtIndex:indexPath.row-1]];
        }
        
        [txtFieldArray addObject:cell.txtField];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"confirmationCodeVC"]) {
        confirmationCodeVC *VC = (confirmationCodeVC*)[segue destinationViewController];
    }
    else if ([segue.identifier isEqualToString:@"DrawerVC"]) {
        DrawerVC *VC = (DrawerVC*)[segue destinationViewController];
    }
}


@end

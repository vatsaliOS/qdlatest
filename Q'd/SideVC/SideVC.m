//
//  SideVC.m
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "SideVC.h"
#import "SettingsVC.h"
#import "ViewController.h"
#import "SharedClass.h"
#import "MBProgressHUD.h"

@interface SideVC ()
{
    BOOL isLogOut;
}
@end

@implementation SideVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES];
    
    isLogOut = NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
//    [self.navigationController setNavigationBarHidden:YES];

    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [self.navigationController setNavigationBarHidden:NO];
}


#pragma mark TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
//    cell.backgroundColor = [UIColor whiteColor];

    UIImageView *imgView = (UIImageView*)[cell viewWithTag:99];
    UILabel *lbl = (UILabel*)[cell viewWithTag:199];
    
    lbl.textColor = [UIColor darkGrayColor];
    //    cell.cellImgView.image = [UIImage imageNamed:@""];
    
    
    if (indexPath.row == 0) {
        lbl.text = @"Notification settings";
        imgView.image = [UIImage imageNamed:@"Settings"];
    }
    else if (indexPath.row == 1)
    {
        lbl.text = @"Sign out";
        imgView.image = [UIImage imageNamed:@"Logout"];
    }
    
    if (IS_IPHONE_4_OR_LESS) {
        lbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:15.0f];
    }
    else if (IS_IPHONE_5) {
        lbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:18.0f];
    }
    else if (IS_IPHONE_6 || IS_IPHONE_6P)
    {
        lbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:21.0f];
    }

    lbl.textColor = RGBCOLOR(100, 100, 100);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"SettingsVC" sender:nil];     // When Click on Notifications
    }
    else if (indexPath.row == 1){
        
        [self logoutAPI];
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_4_OR_LESS) {
        return 55;
    }
    else if (IS_IPHONE_5){
        return 60;
    }
    else
    {
        return 65;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_4_OR_LESS) {
        return 55;
    }
    else if (IS_IPHONE_5){
        return 60;
    }
    else
    {
        return 65;
    }
}

#pragma mark - LogoutAPI
-(void)logoutAPI
{
//    user[id]
//    user[device_token]
//    user[device_type]

//    if (SharedObj.isNetworkReachable) {
        MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetails"]valueForKey:@"user_id"]] forKey:@"id"];
        [Dict setValue:@"1" forKey:@"device_type"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"DeviceToken"]] forKey:@"device_token"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"user"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/users/logout",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (success)
            {
                NSLog(@"response in success = %@",response);
                
                SharedObj.hasLogin = NO;
                
//                for (UIViewController *VC in self.navigationController.viewControllers) {
//                    if ([VC isKindOfClass:[ViewController class]]) {
//                        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"HasLogin"]; //When Logout is done
//                        
//                        //                DisplayAlertWithTitle(@"User logout successfully", @"Q'd");
//                        
//                        isLogOut = YES;
//                        
//                        [self.navigationController popToViewController:VC animated:YES];
//                        
//                        return;
//                        
//                        
//                        
//                    }
//                }
                
//                if (isLogOut) {
                    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"HasLogin"];
                    [self performSegueWithIdentifier:@"GotoMainVC" sender:nil];
//                }

            }
            else
            {
                DisplayAlertControllerWithTitle(@"Something went wrong , Please try again", @"Q'd");
            }
        }];
//    }
//    else
//    {
//        DisplayAlertControllerWithTitle(@"", @"Q'd");
//    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"GotoMainVC"]) {
        ViewController *VC = (ViewController*)[segue destinationViewController];
    }
    else
    {
        SettingsVC *VC = (SettingsVC*)[segue destinationViewController];
    }
}


@end

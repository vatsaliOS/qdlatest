

#import <UIKit/UIKit.h>

@interface NavigationClassVC : UIViewController

-(void)setLeftBtn:(BOOL)isPushView;
-(void)setLeftBtnWithImageName:(NSString *)imageName;
-(void)setLeftBtnWithImageName:(NSString *)imageName andLSelector:(SEL)LSelector;
-(void)setRightBtn1:(NSString*)imageName andRightSelector:(SEL)Rselector and1Button:(NSString*)imageName1 andRight1Selector:(SEL)R1Selector withNotiCount:(int)notiCount;

-(void)setRightBtn:(NSString*)imageName andRightSelector:(SEL)Rselector andWith:(BOOL)shouldDisplayRightBtn;

//-(void)setLeftBtnWithImageName:(NSString *)imageName andLSelector:(SEL)LSelector;
-(void)setLeftBtnInListVIewVC:(NSString *)imageName andLSelector:(SEL)LSelector;

-(void)showNavigationbar;

@end

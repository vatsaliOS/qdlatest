//
//  confirmationCodeVC.m
//  Qd
//
//  Created by SOTSYS028 on 19/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "confirmationCodeVC.h"
#import "MBProgressHUD.h"
#import "Common.h"
#import "DrawerVC.h"

@interface confirmationCodeVC ()
{
    IBOutlet UIView *confirmationView;
}
@property (weak, nonatomic) IBOutlet UITextField *confirmcodeTxtField;
@end

@implementation confirmationCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [confirmationView.layer setCornerRadius:5.0f];
    [confirmationView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [confirmationView.layer setBorderWidth:1.0f];
}

-(IBAction)createAccountBtnClk:(id)sender
{
    [self.confirmcodeTxtField resignFirstResponder];
    
    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"UserDetails"] objectForKey:@"user_id"]] forKey:@"user_id"];
    [Dict setValue:[NSString stringWithFormat:@"%@",self.confirmcodeTxtField.text] forKey:@"verification_code"];
    
    NSMutableDictionary *mainDict = [NSMutableDictionary new];
    [mainDict setObject:Dict forKey:@"user"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/auth/verificationcode",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            
//          DisplayAlertControllerWithTitle(@"SignUp successfully", @"Q'd");
            
            NSString *text = [response valueForKey:@"message"];
            DisplayAlertWithTitle(text, @"Q'd");
            
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"HasLogin"];
            [self performSegueWithIdentifier:@"DrawerVC" sender:nil];

//            [self.navigationController popToRootViewControllerAnimated:YES];

        }
        else
        {
            NSString *responseMessage = [response valueForKey:@"message"];
            DisplayAlertControllerWithTitle(responseMessage, @"Q'd");
        }
    }];

}
- (IBAction)resendVerificationCode:(id)sender {
    
    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"UserDetails"] objectForKey:@"user_id"]] forKey:@"user_id"];
    
    NSMutableDictionary *mainDict = [NSMutableDictionary new];
    [mainDict setObject:Dict forKey:@"user"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/auth/resend_verification_code",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
        }
        else
        {
            NSString *responseMessage = [response valueForKey:@"message"];
            DisplayAlertControllerWithTitle(responseMessage, @"Q'd");
        }
    }];

}

-(IBAction)backBtnClk:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"DrawerVC"]) {
        DrawerVC *VC = (DrawerVC*)[segue destinationViewController];
    }
}


@end

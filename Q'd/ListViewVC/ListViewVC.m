//
//  ListViewVC.m
//  Q'd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "ListViewVC.h"
#import "ListViewCell.h"
#import "DetailBistroVC.h"
#import <UIViewController+MMDrawerController.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>

#import "Common.h"
#import "MBProgressHUD.h"
#import <UIImageView+WebCache.h>
#import "AppDelegate.h"

#import "CallOutView.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"

//static CLLocationDistance const kFixedRadius = 10;

@interface ListViewVC ()
{
    IBOutlet UITableView *tblView;
    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet MKMapView *mapView;
    __weak IBOutlet MKMapView *mapViewFullScreen;
    BOOL isFiltered;
    __weak IBOutlet UIButton *mapViewBtn;
    __weak IBOutlet UIImageView *fadeImgView;
    BOOL showFullMap;
    
    NSMutableArray *venuesArray;
    CLLocationManager *locationManager;
    int pageOffset;
    BOOL isPageRefresing;
    BOOL isMorePage;
    CLLocationCoordinate2D coord;

    BOOL isSearchClicked;
    int searchPageOffset;
    
    __weak IBOutlet UIView *transparentViewAboveTbl;
    
    UIButton *btnRight;     // MapViewBtn  , i.e RightBarbutton
    
    BOOL isViewDidLayoutSubviews;
    BOOL isCalledVenuesAPI;
    double latitude;
    double longitude;
    
    BOOL isFromBackground;
    __weak IBOutlet NSLayoutConstraint *mapViewHeightConstraint;
    
    int venuesCount;
    __weak IBOutlet NSLayoutConstraint *tblViewwithBottomLayoutConstraint;
    __weak IBOutlet NSLayoutConstraint *tblViewHeightConstraint;
    
    IBOutlet UIView *callOutView;
    __weak IBOutlet UILabel *callOutViewLbl;
    
    BOOL isSearchingStart;
    __weak IBOutlet UIButton *callOutBtn;
    NSIndexPath *callOutBtnIndexPath;
    
    BOOL isCallOutBtnClick;
    
    UIView *customCallOutView;
    IBOutlet UIImageView *callOutImgView;
    
    NSMutableArray  *tmpVenuesArray;
    
}
@end

@implementation ListViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isCallOutBtnClick = NO;     // Added on 12-3
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(methodCalled) name:@"methodCalled" object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard1)];
    [mapView addGestureRecognizer:tap];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard1)];
    
    [mapViewFullScreen addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(dismissKeyboard1)];
    
    [transparentViewAboveTbl addGestureRecognizer:tap2];
    
    // 22-Jan

    //
    // added In Last on 22-1-2016 ....// Use this when using custom Navigation bar
    //    [self showNavigationbar];
    
    //    [self.navigationController setNavigationBarHidden:YES];
    //    [self setLeftBtnInListVIewVC:@"sidebar" andLSelector:@selector(sideBtnClk:)];
    
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.bounds = CGRectMake( 35, 20, 25, 25);
    [btnLeft addTarget:self action:@selector(sideBtnClk:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnLeft setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
    
    [btnLeft setBackgroundImage:[UIImage imageNamed:@"sidebar"] forState:UIControlStateNormal];
    [btnLeft.imageView setContentMode:UIViewContentModeCenter];
    
//  UIImageView *imgView = [[UIImageView alloc]init ];
//  imgView.bounds = CGRectMake(10, 30, 29, 24);
//  [imgView setImage:[UIImage imageNamed:@"sidebar"]];
//  [btnLeft addSubview:imgView];
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
    self.navigationItem.leftBarButtonItem = backButton1;
    
//    UIBarButtonItem *negativeSpacerLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
//    [negativeSpacerLeft setWidth:-20];
//    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:negativeSpacerLeft,backButton1,nil];
    //    [btnLeft setTitle:@"BACK" forState:UIControlStateNormal];
    
    CGRect frame = CGRectMake(120, 0, 50, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    if (IS_IPHONE_5) {
        label.font = [UIFont fontWithName:@"HindVadodara-Bold" size:18.0f];
    }
    else if(IS_IPHONE_6 || IS_IPHONE_6P)
    {
        label.font = [UIFont fontWithName:@"HindVadodara-Bold" size:22.0f];
    }
        
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Q'd";
    self.navigationItem.titleView = label;
//    [self.navigationItem setTitle:@"Q'd"];
    
    btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnRight addTarget:self action:@selector(mapViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
        if ([SharedObj.mapViewBtnTitleLblText isEqualToString:@"LIST VIEW"]) {      // Added on 29-3
            [btnRight setTitle:@"LIST VIEW" forState:UIControlStateNormal];
        }
        else
        {
            [btnRight setTitle:@"MAP VIEW" forState:UIControlStateNormal];
        }
    
//    [btnRight setTitle:@"MAP VIEW" forState:UIControlStateNormal];    // commented on 29-3
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        btnRight.bounds = CGRectMake(self.view.frame.size.width-75, 20, 82, 44);
        [btnRight.titleLabel setFont:[UIFont fontWithName:@"HindVadodara-Light" size:18.0f]];
    }
    else if(IS_IPHONE_6 || IS_IPHONE_6P)
    {
        btnRight.bounds = CGRectMake(self.view.frame.size.width-95, 20, 102, 44);
        [btnRight.titleLabel setFont:[UIFont fontWithName:@"HindVadodara-Light" size:22.0f]];
    }

    [btnRight.titleLabel setTextAlignment:NSTextAlignmentRight];
    [btnRight setSelected:NO];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
    self.navigationItem.rightBarButtonItem = backButton;
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [negativeSpacer setWidth:-10];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer,backButton,nil];
    
    
    self.navigationController.navigationBar.barTintColor = RGBCOLOR(255, 0, 78);
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    searchBar.layer.borderWidth = 1;
    searchBar.layer.borderColor = [RGBCOLOR(181, 10, 53) CGColor];
    
    // till here

    // Notification For Fade ImgView
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showFadeImg:) name:@"ShowFadeImg" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideFadeImg:) name:@"HideFadeImg" object:nil];

    // Using the drawer

    [venuesArray removeAllObjects];
    venuesArray= [[NSMutableArray alloc]init];
    
    [tmpVenuesArray removeAllObjects];
    tmpVenuesArray = [[NSMutableArray alloc]init];  // Added on 28-3
    
//    locationManager = [[CLLocationManager alloc] init];
    
    latitude = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"latitude"]doubleValue];
    longitude = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"longitude"]doubleValue];
    
    coord.longitude = latitude;
    coord.latitude = longitude;
    
    pageOffset = 1;

    // For Search
    isSearchClicked = NO;
    
//    [[appDelegate locationManager] requestWhenInUseAuthorization];
//    [[appDelegate locationManager] requestAlwaysAuthorization];
    
//    locationManager = [[CLLocationManager alloc]init];
    
    [appDelegate locationManager].delegate = self;
//    [appDelegate locationManager].desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
//    [appDelegate locationManager].distanceFilter = kFixedRadius;
    
    
//    double latitude = [[self.restaurantDetailDict valueForKey:@"latitude"] doubleValue];
//    double longitude = [[self.restaurantDetailDict valueForKey:@"longitude"] doubleValue];
    
    // setup the map pin with all data and add to map view
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MKCoordinateRegion region = mapView.region;

        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        CLLocation *location = [[appDelegate locationManager] location];
        coordinate.longitude = location.coordinate.longitude;
        coordinate.latitude = location.coordinate.latitude;
        
        region.center = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
        region.span.longitudeDelta /= 3000.0; // Bigger the value, closer the map view
        region.span.latitudeDelta /= 3000.0;
        [mapView setRegion:region animated:YES]; // Choose if you want animate or not
        [mapViewFullScreen setRegion:region animated:YES];
    });
   
    
//    [appDelegate locationManager].distanceFilter = kFixedDistanceFilter;
    
//    [self VenuesAPI]; // commented on 2-2-2016
//    [[appDelegate locationManager] startUpdatingLocation];        // commented on 11-2-2016

    isFromBackground = NO;  // Added on 8-2-2016

    // Added on 12-2-2016
    
    
    
//    [[appDelegate locationManager] stopUpdatingLocation];
}

-(void)dismissKeyboard1 {
    
    [transparentViewAboveTbl setHidden:YES];        // Added on 28-1-2016
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
    
    searchBar.text = @"";   // Added on 18-3
    
    if (!isSearchingStart) {    //  Added on 19-3
        
//        if (isSearchingStart) {    // commented on 19-3

//        isCallOutBtnClick = NO;     // Added on 12-3
        
//        if (tblViewwithBottomLayoutConstraint == 0) {   // this if condition i sadded on 19-3
//            [venuesArray removeAllObjects];
//            venuesArray= [[NSMutableArray alloc]init];
//            pageOffset = 1;
//            [self VenuesAPI];
//        }
        
//        if (tblViewwithBottomLayoutConstraint != 0) {     // commented on 19-3
            tblViewwithBottomLayoutConstraint.constant = 0;
            float tblHeightMultipler = (368*self.view.frame.size.height)/600;
            tblViewHeightConstraint.constant = (self.view.frame.size.height -tblHeightMultipler+80); //368-108;
//        }
        
        if(isSearchClicked)     // Condition added on 19-3
        {
            [venuesArray removeAllObjects];
            venuesArray= [[NSMutableArray alloc]init];
            pageOffset = 1;
            [self VenuesAPI];
        }
        
        isSearchingStart = NO;
        
    }
    
//    // Added on 12-3
//    if (isCallOutBtnClick) {
//        [self performSegueWithIdentifier:@"DetailBistroVC" sender:callOutBtnIndexPath];
//    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    [transparentViewAboveTbl setHidden:YES];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [fadeImgView setHidden:YES];
}

// Added on 3-2-2016
-(void)viewDidLayoutSubviews
{
    if (!isViewDidLayoutSubviews) {
        
        
//        [[appDelegate locationManager] startUpdatingLocation];        // commented on 11-2-2016

        isCallOutBtnClick = NO;    // Added on 12-3
        
        searchBar.text = @"";
        
        isFromBackground = NO;      // Added on 8-2-2016

        [venuesArray removeAllObjects];
        venuesArray= [[NSMutableArray alloc]init];
        pageOffset = 1;
        
//        [tmpVenuesArray removeAllObjects];
//        tmpVenuesArray = [[NSMutableArray alloc]init];  // Added on 28-3
        
        // commented on 3-2-2016 at 4:30
//        if (venuesArray.count>0)
//        {
//            [tblView scrollsToTop];
//        }
        
        [transparentViewAboveTbl setHidden:YES];
        [fadeImgView setHidden:YES];
            
        [self VenuesAPI];
        isViewDidLayoutSubviews = YES;
        
        
        // added from here on 10-3
                
        if ([SharedObj.mapViewBtnTitleLblText isEqualToString:@"LIST VIEW"]) {      // When Full Map Sreen is open
            float tblViewHeight = 0;
            tblViewHeight = -tblView.frame.size.height-8;
            
            NSLog(@"tblView.frame.size.height == ==== %f",tblView.frame.size.height);

            tblViewwithBottomLayoutConstraint.constant = tblViewHeight;
            
//            [btnRight setTitle:@"LIST VIEW" forState:UIControlStateNormal];     // Added on 29-3
            
        }
        else
        {
            NSLog(@"Before in First time == %f",tblView.frame.size.height);
            
//            tblViewHeightConstraint.constant = tblView.frame.size.height * 0.6133 + mapView.frame.size.height-36;

            // Added on 10-3
            float tblHeightMultipler = (368*self.view.frame.size.height)/600;
            
            tblViewHeightConstraint.constant = (self.view.frame.size.height -tblHeightMultipler+80); //368-108;
            
            // till here
            isSearchClicked = NO;   // added on 19-3
            
//            [btnRight setTitle:@"MAP VIEW" forState:UIControlStateNormal];  // Added on 29-3

//            float heightConstant = 0;
//            float tempViewTop = 0;
//            
//            float tblViewHeight = 0;
//            
//            if (btn.isSelected) {
//                
//                isSearchClicked = NO;
//                
//                // commented on 8-2-2016
//                
//                //        pageOffset = 1;
//                //        [venuesArray removeAllObjects];
//                //        venuesArray= [[NSMutableArray alloc]init];
//                //        [self VenuesAPI];
//                
//                // till here
//                
//                
//                //        heightConstant = -8;              // Added on 9-2-2016
//                
//                //        tempViewTop = -8.0f;        // Added on 11-2-2016
//                
//                [btnRight setTitle:@"MAP VIEW" forState:UIControlStateNormal];
//                
//                tblViewHeight = 0;
//                
//                //        [btnRight setTitle:@"MAP VIEW" forState:UIControlStateNormal];  // Added
//                //        [mapViewFullScreen setHidden:YES];    // commented on 9-2-2016
//                
//            }
//            else
//            {
//                [btnRight setTitle:@"LIST VIEW" forState:UIControlStateNormal];
//                //        [btnRight setTitle:@"LIST VIEW" forState:UIControlStateNormal];
//                showFullMap = YES;
//                
//                heightConstant = self.view.frame.size.height-mapView.frame.size.height-52;       // Added on 9-2-2016
//                tempViewTop = -8.0f;         // Added on 11-2-2016
//                
//                tblViewHeight = -tblView.frame.size.height-8;
//                
//                
//                //        [mapViewFullScreen setHidden:NO];     // commented on 9-2-2016
//                [btnRight setSelected:YES];
//            }

        }
        // till here on 10-3
        
//        tblViewHeightConstraint.constant = tblView.frame.size.height * 0.6133 + mapView.frame.size.height-36; // commented on 10-3

    }
}
// till here


#pragma mark - VenuesAPI

-(void)VenuesAPI
{
    // Added on 3-2-2016
    if (!isCalledVenuesAPI) {       // If condition is added on 3-2-2016 , so that , this method won't get called multiple times
        
        if (isFromBackground) {
            
        }
        else
        {
            MKCoordinateRegion region = mapView.region;
            CLLocation *location = [[appDelegate locationManager] location];
            longitude = location.coordinate.longitude;
            latitude = location.coordinate.latitude;
            
            NSLog(@"latitude = %f & longitude == %f",latitude , longitude);
            
            // commented on 14-3
            
//            region.center = CLLocationCoordinate2DMake(latitude, longitude);
//            [mapView setRegion:region animated:YES]; // Choose if you want animate or not
//            [mapViewFullScreen setRegion:region animated:YES];
            
            // till here on 14-3
        }
        
        isFromBackground = NO;

//        MKCoordinateRegion region = mapView.region;
//        CLLocation *location = [[appDelegate locationManager] location];
//        longitude = location.coordinate.longitude;
//        latitude = location.coordinate.latitude;
//        
//        NSLog(@"latitude = %f & longitude == %f",latitude , longitude);
//        
//        region.center = CLLocationCoordinate2DMake(latitude, longitude);
//        [mapView setRegion:region animated:YES]; // Choose if you want animate or not
//        [mapViewFullScreen setRegion:region animated:YES];
//        isFromBackground = NO;
        
        // till here
        isCalledVenuesAPI = YES;
        
        NSLog(@"Called Venues API");
        
        MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%f",latitude]forKey:@"latitude"];
        [Dict setValue:[NSString stringWithFormat:@"%f",longitude] forKey:@"longitude"];
        [Dict setValue:[NSString stringWithFormat:@"%d",pageOffset] forKey:@"page"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetails"]valueForKey:@"user_id"]] forKey:@"user_id"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/users/getvenues/",BASEURL] withParam:Dict withCompletion:^(NSDictionary *response, BOOL success){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                isCalledVenuesAPI = NO;
            });
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
            
            if (success)
            {
//                NSLog(@"response in success = %@",response);
                //            venuesArray = [response valueForKey:@"venues"];
                
                [venuesArray addObjectsFromArray:[response valueForKey:@"venues"]];
                
                [[NSUserDefaults standardUserDefaults]setObject:[venuesArray mutableCopy] forKey:@"venuesArray"];       //Added on 28-3
                
                //            DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                
                [mapView removeAnnotations:mapView.annotations];
                [mapViewFullScreen removeAnnotations:mapViewFullScreen.annotations];
                
                for(int i = 0; i < venuesArray.count; i++)
                {
                    [self addPinWithTitle:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"name"]] AndLatitude:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"latitude"]] AndLongitude:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"longitude"]]];
                }
                
                NSLog(@"venues_count == %d",[[response valueForKey:@"venues_count"] intValue]);
                
                venuesCount = [[response valueForKey:@"venues_count"] intValue];        // Added on 11-2-2016
                
                if ([[response valueForKey:@"venues_count"] intValue]%10 >= 0) {
                    isMorePage=YES;
                }
                else
                {
                    isMorePage=NO;
                }
                
                [tblView reloadData];
                isPageRefresing=NO;
                
            }
            else
            {
                [venuesArray removeAllObjects];
                venuesArray = [[NSMutableArray alloc]init];
                [tblView reloadData];
                DisplayAlertControllerWithTitle(@"Something went wrong Please try again", @"Q'd");

            }
            
        }];

    }
   
}

#pragma mark - AddMultiplePins

-(void)addPinWithTitle:(NSString *)title AndLatitude:(NSString *)strLatitude AndLongitude:(NSString*)strLongitude
{
    MKPointAnnotation *mapPin = [[MKPointAnnotation alloc] init];
    
//    // clear out any white space
//    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//    // convert string into actual latitude and longitude values
//    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
    
    double latitude = [strLatitude doubleValue];
    double longitude = [strLongitude doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    mapPin.title = title;
    mapPin.coordinate = coordinate;
    
//    static NSString *reuseId = @"asdf";
//    
//    MKPinAnnotationView *pin = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: reuseId];
//    if (pin == nil)
//        pin = [[MKPinAnnotationView alloc] initWithAnnotation:mapPin reuseIdentifier: reuseId];
//    else
//        pin.annotation = mapPin;
//    
////    pin.annotation = mapPin;
//    pin.pinTintColor = [UIColor purpleColor];
    
    [mapView addAnnotation:mapPin];
    [mapViewFullScreen addAnnotation:mapPin];   // for fullscreen map
}

#pragma mark - MapViewDelegate

-(MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id<MKAnnotation>)annotation
{
//    MKPinAnnotationView *pin = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: @"asdf"];
//    pin.annotation = annotation;
//    pin.pinTintColor = [UIColor purpleColor];
//    
//    return pin;
    
    static NSString *reuseId = @"currentloc";
    
    MKPinAnnotationView *annView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (annotation == mapView.userLocation || annotation == mapViewFullScreen.userLocation) {
        return nil;
    }
    if (annView == nil)
    {
        
        annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        annView.animatesDrop = NO;
        annView.canShowCallout = NO;        // changed from YES to NO on 11-3
        annView.calloutOffset = CGPointMake(-5, 5);
        
    }
    else
    {
        annView.annotation = annotation;
    }
    annView.pinColor = MKPinAnnotationColorPurple;
 
    return annView;
    
    
    
//    UIImageView *pinView = nil;
//    
//    DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([DXAnnotationView class])];
//    
//    if (annotation == mapView.userLocation || annotation == mapViewFullScreen.userLocation) {
//        return nil;
//    }
//
//    if (!annotationView) {
//        
//        pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Pin"]];
//        
//        callOutView = [[[NSBundle mainBundle] loadNibNamed:@"CallOutView" owner:self options:nil] firstObject];
//        
////        CGRect calloutViewFrame = CGRectMake(0, 0, 250, 50);
////        calloutViewFrame.origin = CGPointMake(-calloutViewFrame.size.width/2, -calloutViewFrame.size.height);
////        [callOutView setUserInteractionEnabled:TRUE];
////        callOutView.frame = calloutViewFrame;
//
//        [annotationView addSubview:callOutView];
//        
//        annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
//                                                      reuseIdentifier:NSStringFromClass([DXAnnotationView class])
//                                                              pinView:pinView
//                                                          calloutView:callOutView
//                                                             settings:[DXAnnotationSettings defaultSettings]];
//    }
//    else {
//        //Changing PinView's image to test the recycle
////        pinView = (UIImageView *)annotationView.pinView;
////        pinView.image = [UIImage imageNamed:@"pin"];
//    }
//
//    return annotationView;

}

-(void)mapView:(MKMapView *)mapViewIn didSelectAnnotationView:(MKAnnotationView *)view
{
    if (view.annotation == mapViewIn.userLocation) {
        for (UIView *subview in view.subviews ){
            [subview removeFromSuperview];
        }
    }
    else
    {
        CGRect calloutViewFrame = CGRectMake(0, 0, 250, 65);
        calloutViewFrame.origin = CGPointMake(-calloutViewFrame.size.width/2+5, -calloutViewFrame.size.height-2);
        [callOutView setUserInteractionEnabled:TRUE];
        callOutView.frame = calloutViewFrame;

        CallOutView *topView = [[CallOutView alloc] initWithFrame:CGRectMake(0, 00, 250, 65)];
        topView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:topView];
        
        topView.underneathButton = callOutBtn;
        
        [view addSubview:callOutView];
        
        id<MKAnnotation> ann = [[mapView selectedAnnotations] objectAtIndex:0];
        NSString *strTitle = ann.title;
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"SELF['name'] CONTAINS %@",strTitle];
                
        NSArray *arrPopup = [venuesArray filteredArrayUsingPredicate:predicate];
        
        NSLog(@"arrPopup = %@",arrPopup);
        
        
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:callOutImgView.bounds
                                         byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerTopLeft)
                                               cornerRadii:CGSizeMake(7.0, 7.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = callOutImgView.bounds;
        maskLayer.path = maskPath.CGPath;
        callOutImgView.layer.mask = maskLayer;
        
        callOutViewLbl.text = [NSString stringWithFormat:@"%@",[[arrPopup objectAtIndex:0]valueForKey:@"name"]];
        [callOutImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrPopup objectAtIndex:0]valueForKey:@"image"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        
        NSInteger indexOfTheObject = [venuesArray indexOfObject:[arrPopup objectAtIndex:0]];
//        NSLog(@"indexOfTheObject == %d",indexOfTheObject);

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfTheObject inSection:0];
        NSLog(@"indexPath.row == %@",indexPath);
        
        callOutBtnIndexPath = indexPath;
        
        isCallOutBtnClick = YES;    // Added on 12-3
        // till here
    }
    
}



-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    [callOutView removeFromSuperview];
}

//-(CLLocationCoordinate2D) getLocation{
//    
//    CLLocationCoordinate2D coordinate;// = [location coordinate];
//    
//    if ([CLLocationManager locationServicesEnabled] == YES) {
//        locationManager = [[CLLocationManager alloc] init];
//        locationManager.delegate = self;
//        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
//        locationManager.distanceFilter = kCLDistanceFilterNone;
//        locationManager.activityType = CLActivityTypeAutomotiveNavigation;
//#ifdef __IPHONE_8_0
////        if(IS_IOS8_AND_UP) {
//            // Use one or the other, not both. Depending on what you put in info.plist
//            // [locationManager requestWhenInUseAuthorization];
//            [locationManager requestAlwaysAuthorization];
////        }
//#endif
//        [locationManager startUpdatingLocation];
//        
//        CLLocation *location = [locationManager location];
//        coordinate = [location coordinate];
//    }
//    return coordinate;
//}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
//    
//    mapView.showsUserLocation=TRUE;
//    mapView.userLocation.title = @"Current Location";
//    mapView.delegate = self;
//    
//    region.center.latitude =newLocation.coordinate.latitude;
//    region.center.longitude =newLocation.coordinate.longitude;
//    region.span.longitudeDelta = 0.0002;
//    region.span.latitudeDelta = 0.0002;
//    [mapView setRegion:region animated:NO];
//    
//    [locationManager stopUpdatingLocation];
//}

#pragma mark - Location Manager delegate
// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
//    [[appDelegate locationManager] startUpdatingLocation];  // changed from Stop to Start

//    MKCoordinateRegion region;
//    region.center = mapView.userLocation.coordinate;
//    region.span = MKCoordinateSpanMake(0.5, 0.5); //Zoom distance
////    region.span.longitudeDelta /= 500.0; // Bigger the value, closer the map view
////    region.span.latitudeDelta /= 500.0;
//    [mapView setRegion:region animated:YES]; // Choose if you want animate or not
//    [mapViewFullScreen setRegion:region animated:YES];
    
//    self.centerToUserLocation = NO;
//    NSLog(@"%@", [locations lastObject]);
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(methodCalled) name:UIApplicationWillEnterForegroundNotification object:nil];
    
//    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"latitude"]] forKey:@"latitude"];
//    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"longitude"]] forKey:@"longitude"];
    
//    NSString *distance = [NSString stringWithFormat:@"%f",locationManager.distanceFilter];
    
//    DisplayAlertWithTitle(@" Before Condition", @"Hello");

    if (!isCalledVenuesAPI) {
        
        NSLog(@"In did Update location");   
        
//        DisplayAlertWithTitle(@"Called Did Update Location inside Condition", @"Success");
        
//        if(![searchBar.text isEqualToString:@""] || isSearchingStart == YES)   // Added this If condition on 18-3 , commented on 29-3
//        {
            pageOffset = 1;
            [venuesArray removeAllObjects];
            venuesArray= [[NSMutableArray alloc]init];
            [self VenuesAPI];   // Added on 2-2-2016
//        }
       
    }
    
//    [[appDelegate locationManager] stopUpdatingLocation];   // Added on 2-2 at 4:15
    

//    }

}

-(void)methodCalled{
    NSLog(@"Method called");
    
    // Added on 8-2-2016
    
    isFromBackground = YES;
    if (isFromBackground) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            MKCoordinateRegion region = mapView.region;
            CLLocation *location = [[appDelegate locationManager] location];
            longitude = location.coordinate.longitude;
            latitude = location.coordinate.latitude;
            region.center = CLLocationCoordinate2DMake(latitude, longitude);
            [mapView setRegion:region animated:YES]; // Choose if you want animate or not
            [mapViewFullScreen setRegion:region animated:YES];
            
            pageOffset = 1;
            [venuesArray removeAllObjects]; // 3-2-2016
            venuesArray= [[NSMutableArray alloc]init];
            isCalledVenuesAPI = NO;
            [self VenuesAPI];     // commented on 2-2 at 4:15
        });
        //    [[appDelegate locationManager] startMonitoringSignificantLocationChanges];  // Added on 8-2-2016
        
        // till here
        
//        pageOffset = 1;
//        [venuesArray removeAllObjects]; // 3-2-2016
//        venuesArray= [[NSMutableArray alloc]init];
//        isCalledVenuesAPI = NO;

    }
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        MKCoordinateRegion region = mapView.region;
//        CLLocation *location = [[appDelegate locationManager] location];
//        longitude = location.coordinate.longitude;
//        latitude = location.coordinate.latitude;
//
//        [self VenuesAPI];     // commented on 2-2 at 4:15
//    });
//  //    [[appDelegate locationManager] startMonitoringSignificantLocationChanges];  // Added on 8-2-2016
//
//    // till here
//    
//    pageOffset = 1;
//    [venuesArray removeAllObjects]; // 3-2-2016
//    venuesArray= [[NSMutableArray alloc]init];
//    isCalledVenuesAPI = NO;
    
//    [[appDelegate locationManager] startUpdatingLocation];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"methodCalled" object:nil];
    
//    [self VenuesAPI];     // commented on 2-2 at 4:15
    
//    [[appDelegate locationManager] stopUpdatingLocation];
}


#pragma mark - TableView Methods

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return venuesArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ListViewCell *cell = (ListViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];

    if (venuesArray.count>0)
    {
        
        cell.backgroundColor = [UIColor whiteColor];
    //    cell.cellImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"demo_img_%d",indexPath.row+1]];
    //    
    //    if (indexPath.row >5) {
    //        cell.cellImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"demo_img_2"]];
    //    }
        
            [cell.cellImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:indexPath.row]valueForKey:@"image"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
            }];
            
            cell.titleLbl.text = [NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:indexPath.row]valueForKey:@"name"]];
            cell.addressLbl.text = [NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:indexPath.row]valueForKey:@"address"]];
            cell.distanceLbl.text = [NSString stringWithFormat:@"< %dkm",[[[venuesArray objectAtIndex:indexPath.row]valueForKey:@"distance"] intValue]];
        
        if (IS_IPHONE_4_OR_LESS){
            [cell.addressLbl setFont:[UIFont fontWithName:@"HindVadodara-Light" size:14.0]];
        }
        else if (IS_IPHONE_5){
            [cell.addressLbl setFont:[UIFont fontWithName:@"HindVadodara-Light" size:15.0]];
        }
        else
        {
            [cell.addressLbl setFont:[UIFont fontWithName:@"HindVadodara-Light" size:18.0]];
        }
        
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    searchBar.showsCancelButton = NO;   //Added on 28-3
    [searchBar resignFirstResponder];
    
    [self performSegueWithIdentifier:@"DetailBistroVC" sender:indexPath];
}

#pragma mark - SearchBar methods

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSLog(@"Yes");
    
    isSearchingStart = YES;     // Added on 11-3
    
    if (!showFullMap) {
        
//        [self.view bringSubviewToFront:transparentViewAboveTbl];  // commented on 28-3
        [self.view bringSubviewToFront:tblView];    // Added on 28-3
    }
    
    // commented on 15-3

    
//    // Added on 10-3
//    [venuesArray removeAllObjects];
//    venuesArray= [[NSMutableArray alloc]init];
//    pageOffset = 1;
//    [tblView reloadData];
//    // till here
    
    
//    [venuesArray removeAllObjects];
//    venuesArray = [[NSMutableArray alloc]init];
//    [tblView reloadData];
    // till here

    [transparentViewAboveTbl setHidden:NO];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar1
{
    if (!showFullMap) {
        // commented on 28-3
//        [tblView bringSubviewToFront:transparentViewAboveTbl];
//        [transparentViewAboveTbl setHidden:YES];
        // till here

    }
    [searchBar resignFirstResponder];   // Added on 11-3
    
//     // Added on 28-3
//    venuesArray = tmpVenuesArray;
//    pageOffset = 1;
//    [tblView reloadData];
//    // till here
    
//    isSearchingStart = NO;     // Added on 11-3, commented on 16-3
    
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    if(searchText.length == 0)
    {
        // commented on 8-2-20216 , then added on 9-2-2016 according to tester
        
        isSearchingStart = YES;     // Added on 16-3
        
        // commented on 28-3
//        [venuesArray removeAllObjects];
//        venuesArray= [[NSMutableArray alloc]init];
//        pageOffset = 1;
        // till here
        
//        [self VenuesAPI];     // commented on 11-3
        
        // till here
        
        isSearchClicked = NO;
        
//        [searchBar resignFirstResponder];

        searchBar.showsCancelButton = NO;

//        if (showFullMap) {
//            [mapViewFullScreen setHidden:NO];
//        }
        
//        self.viewTopConstraint.constant = 0.0f;       // commented on 12-2-2016
        
        tblViewHeightConstraint.constant = tblView.frame.size.height;     // Added on 12-2-2016
        
//        isFiltered = FALSE;   // commented on 28-3
        
        [venuesArray removeAllObjects];
        venuesArray = [[NSMutableArray alloc]init];
        [tblView reloadData];
        
//        [tblView reloadData]; // comented on 28-3
    }
    else
    {
        isSearchingStart = NO;     // Added on 16-3

        
//        // Added on 15-3 , commented on 28-3
//        [venuesArray removeAllObjects];
//        venuesArray= [[NSMutableArray alloc]init];
//        [tblView reloadData];
        
//        // till here
        
        isSearchClicked = YES;
        searchBar.showsCancelButton = YES;
        
//        [filteredTableData removeAllObjects];
        
        isFiltered = true;
        [mapViewFullScreen setHidden:YES];
        
//        self.viewTopConstraint.constant = -mapView.frame.size.height;     // commented on 12-2-2016
        
        // Added on 10-3
        if(tblViewHeightConstraint.constant == self.view.frame.size.height - 44)
        {
            
        }
        else
        {
            tblViewHeightConstraint.constant = tblView.frame.size.height + mapView.frame.size.height;     // Added on 12-2-2016

        }
        // till here on 10-3
        
        
//        tblViewHeightConstraint.constant = tblView.frame.size.height + mapView.frame.size.height;     // Added on 12-2-2016


        
//        filteredTableData = [[NSMutableArray alloc] init];
//        
//        int i=0;
//        for (NSString *str in [poolTypesArray valueForKey:@"pooltype"])
//        {
//            NSRange nameRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
//            NSRange descriptionRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
//            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
//            {
//                
//                [filteredTableData addObject:[poolTypesArray objectAtIndex:i]];
//            }
//            i++;
//        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        // commented on 29-3
//        [venuesArray removeAllObjects];
//        venuesArray = [[NSMutableArray alloc]init];
//
//        [tblView reloadData];   // Added on 29-3
        // till here
        
        [self searchAPI];   // Added on 28-3

    }
    
//    NSLog(@"filteredTableData == %@",filteredTableData);
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    isSearchingStart = NO;      // Added on 11-3
    
    searchBar.showsCancelButton = NO;

//    if (showFullMap) {
//        [mapViewFullScreen setHidden:NO];
//    }
    
//    self.viewTopConstraint.constant = 0.0f;
    
//    tblViewHeightConstraint.constant = tblView.frame.size.height*0.6133;     // Added on 12-2-2016 , commented on 10-3
    
    // Added on 10-3
    float tblHeightMultipler = (368*self.view.frame.size.height)/600;
    
    tblViewHeightConstraint.constant = (self.view.frame.size.height -tblHeightMultipler+80); //368-108;
    
    // till here
    
    searchBar.text = @"";
    
    [searchBar resignFirstResponder];
    pageOffset  = 1;
    isSearchClicked = NO;

    // Added on 10-3 , commented on 28-3
//    [venuesArray removeAllObjects];
//    venuesArray= [[NSMutableArray alloc]init];
//    pageOffset = 1;
//    [self VenuesAPI];
//    [tblView reloadData];
    // till here
    
    // Added on 28-3
    venuesArray = [[[NSUserDefaults standardUserDefaults]objectForKey:@"venuesArray"]mutableCopy];
    pageOffset = 1;
    [tblView reloadData];
    // till here
    
    // commented on 8-2-2016
    
//    [venuesArray removeAllObjects];
//    venuesArray= [[NSMutableArray alloc]init];
//    [self VenuesAPI];   // When there is no search then show Default API
    
    // till here
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    searchBar.showsCancelButton = NO;
    isSearchClicked = YES;
    [searchBar resignFirstResponder];
    
    isSearchingStart = NO;

    // commented on 8-2-2016 , then added on 9-2-2016
    
    tblViewHeightConstraint.constant = tblView.frame.size.height + mapView.frame.size.height;     // Added on 12-2-2016
    
    NSLog(@"tblViewHeightConstraint.constant == %f",tblViewHeightConstraint.constant);
    
    pageOffset = 1;
    [venuesArray removeAllObjects];
    venuesArray= [[NSMutableArray alloc]init];
    
    // till here
    
    [transparentViewAboveTbl setHidden:YES];        // Added on 28-1
    
    
    [self searchAPI];
}

#pragma mark - SearchAPI

-(void)searchAPI
{
    // Added on 3-2-2016
    MKCoordinateRegion region = mapView.region;
    CLLocation *location = [[appDelegate locationManager] location];
    longitude = location.coordinate.longitude;
    latitude = location.coordinate.latitude;
    region.center = CLLocationCoordinate2DMake(latitude, longitude);
    
    // till here
    
    [venuesArray removeAllObjects];
    venuesArray = [[NSMutableArray alloc]init];
    [tblView reloadData];   // Added on 29-3
    
    
    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%f",latitude] forKey:@"latitude"];
    [Dict setValue:[NSString stringWithFormat:@"%f",longitude] forKey:@"longitude"];
    [Dict setValue:[NSString stringWithFormat:@"%d",pageOffset] forKey:@"page"];
    [Dict setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
    [Dict setValue:searchBar.text forKey:@"query"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/users/search/",BASEURL] withParam:Dict withCompletion:^(NSDictionary *response, BOOL success){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            
            //            venuesArray = [response valueForKey:@"venues"];
            
//            if (![[response valueForKey:@"venues_count"] isEqualToNumber:[NSNumber numberWithInt:0]]) {   // commented on 28-3
            
               
                
                [venuesArray addObjectsFromArray:[response valueForKey:@"venues"]];
                
                //            DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                
                [mapView removeAnnotations:mapView.annotations];
                [mapViewFullScreen removeAnnotations:mapViewFullScreen.annotations];
                
                venuesCount = [[response valueForKey:@"venues_count"] intValue];        // Added on 11-2-2016
                
                for(int i = 0; i < venuesArray.count; i++)
                {
                    [self addPinWithTitle:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"name"]] AndLatitude:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"latitude"]] AndLongitude:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"longitude"]]];
                }
                
                NSLog(@"venues_count == %d",[[response valueForKey:@"venues_count"] intValue]);
                
                if ([[response valueForKey:@"venues_count"] intValue]%5 >= 0) {
                    isMorePage=YES;
                }
                else
                {
                    isMorePage=NO;
                }
                
                [tblView reloadData];
                isPageRefresing=NO;
            
                isSearchingStart = YES;     // Added on 28-3

//            }
            
            
            /*      commented on 28-3
            if (venuesArray.count == 0) {
                // Added on 10-3
                [venuesArray removeAllObjects];
                venuesArray= [[NSMutableArray alloc]init];
                pageOffset = 1;
//                [self VenuesAPI];
                [tblView reloadData];
                // till here
                
                [searchBar becomeFirstResponder];
                searchBar.showsCancelButton = YES;
                DisplayAlertControllerWithTitle(@"No data found", @"Q'd");
            }
             
             */
        }
        else
        {
            
        }
    }];

}

-(void)viewWillDisappear:(BOOL)animated
{
    pageOffset = 1;
    [searchBar resignFirstResponder];
}

-(IBAction)mapViewBtnClick:(UIButton*)sender
{
    [searchBar resignFirstResponder];
    searchBar.showsCancelButton = NO;

//    self.viewTopConstraint.constant = 0.0f;
    searchBar.text = @"";

    UIButton *btn = sender;
    
    float tblViewHeight = 0;

//    if (btn.isSelected) {       // Btn title  is ListView and then button clicked // commented on 29-3
        if ([SharedObj.mapViewBtnTitleLblText isEqualToString:@"LIST VIEW"]) {      // Added on 29-3
            
        isSearchClicked = NO;
        
        // commented on 8-2-2016
        
//        pageOffset = 1;
//        [venuesArray removeAllObjects];
//        venuesArray= [[NSMutableArray alloc]init];
//        [self VenuesAPI];
        
        // till here
        
        
//        heightConstant = -8;              // Added on 9-2-2016
        
//        tempViewTop = -8.0f;        // Added on 11-2-2016
        
        // Added on 10-3 , commented on 28-3
//        [venuesArray removeAllObjects];
//        venuesArray= [[NSMutableArray alloc]init];
//        pageOffset = 1;
//        [self VenuesAPI];
        // till here

        venuesArray = [[[NSUserDefaults standardUserDefaults]objectForKey:@"venuesArray"]mutableCopy];       // Added on 28-3
        [tblView reloadData];
        
        [btnRight setTitle:@"MAP VIEW" forState:UIControlStateNormal];
        
        SharedObj.mapViewBtnTitleLblText = @"MAP VIEW";
        
//        tblViewHeightConstraint.constant = tblView.frame.size.height;     // Added on 10-3-2016
        
        tblViewHeight = 0;
        
//        [btnRight setTitle:@"MAP VIEW" forState:UIControlStateNormal];  // Added
//        [mapViewFullScreen setHidden:YES];    // commented on 9-2-2016
        
        showFullMap = NO;
        [btnRight setSelected:NO];
    }
    else            // btn title is MapView and then button clicked
    {
        SharedObj.mapViewBtnTitleLblText = @"LIST VIEW";
        
        [btnRight setTitle:@"LIST VIEW" forState:UIControlStateNormal];
//        [btnRight setTitle:@"LIST VIEW" forState:UIControlStateNormal];
        showFullMap = YES;
        
//        heightConstant = self.view.frame.size.height-mapView.frame.size.height-52;       // Added on 9-2-2016
//        tempViewTop = -8.0f;         // Added on 11-2-2016
        
        tblViewHeight = -tblView.frame.size.height-8;
        
        venuesArray = [[[NSUserDefaults standardUserDefaults]objectForKey:@"venuesArray"]mutableCopy];       // Added on 28-3
        [tblView reloadData];

        
//        [mapViewFullScreen setHidden:NO];     // commented on 9-2-2016
        [btnRight setSelected:YES];
        
        [tblView setContentOffset:CGPointZero animated:YES];    // Added on 19-3

    }
    
    // Added on 9-2-2016
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        
//        self.mapViewHeightConstraint.constant = heightConstant;
        
        // Added on 10-3
        if (btn.isSelected) {
            tblViewwithBottomLayoutConstraint.constant = tblViewHeight;
        }
        else{
            
            tblViewwithBottomLayoutConstraint.constant = 0;
            
            float tblHeightMultipler = (368*self.view.frame.size.height)/600;
            
            tblViewHeightConstraint.constant = (self.view.frame.size.height -tblHeightMultipler+80); //368-108;

        }
        // till here
        
//        tblViewwithBottomLayoutConstraint.constant = tblViewHeight;

        [self.view layoutIfNeeded];
        
    }completion:^(BOOL finished)
     {
//         self.viewTopConstraint.constant = tempViewTop;
     }];
    // till here
    
}

#pragma mark SideButton CLick
-(IBAction)sideBtnClk:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    if (btn.isSelected) {
        [btn setSelected:NO];
    }
    else
    {
        [btn setSelected:YES];
    }
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    searchBar.showsCancelButton = NO;

    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    
    // Added on 3-2-2016 at 5:00
    NSMutableDictionary *tmpDict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserDetails"] mutableCopy];
    [tmpDict setObject:[NSString stringWithFormat:@"%f",latitude] forKey:@"latitude"];
    [tmpDict setObject:[NSString stringWithFormat:@"%f",longitude] forKey:@"longitude"];
    
    [[NSUserDefaults standardUserDefaults]setObject:tmpDict forKey:@"UserDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // till here
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];

}

#pragma mark Notification Center
-(void)showFadeImg:(NSNotification*)notification
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];      // Added on 18-3

    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [fadeImgView setHidden:NO];
    
    [self.view bringSubviewToFront:fadeImgView];    // added on 15-2-2016
    
    
}

-(void)hideFadeImg:(NSNotification*)notification
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];      // Added on 18-3

    searchBar.showsCancelButton = NO;
    [fadeImgView setHidden:YES];
    [self.view bringSubviewToFront:tblView];
    
//    // Added on 18-3
//
//    [venuesArray removeAllObjects];
//    venuesArray= [[NSMutableArray alloc]init];
//    pageOffset = 1;
//    [self VenuesAPI];
//    float tblHeightMultipler = (368*self.view.frame.size.height)/600;
//    
//    tblViewHeightConstraint.constant = (self.view.frame.size.height -tblHeightMultipler+80); //368-108;
//    
//    //  till here on 18-3
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowFadeImg" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HideFadeImg" object:nil];
}

#pragma mark - ScrollView Delegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height)
    {
        if (!isPageRefresing)
        {
            isPageRefresing=YES;
            pageOffset= pageOffset+1;
            if (isMorePage)
            {
                if (isSearchClicked) {
                    if (venuesArray.count < venuesCount) {      // Added on 11-2-2016
                        [self searchAPI];
                    }
                }
                else
                {
                    if (venuesArray.count < venuesCount) {      // Added on 11-2-2016
                        [self VenuesAPI];
                    }
                }
            }
            
        }
        // we are at the end
    }

}

#pragma mark - CalLOutViewButtonClk

- (IBAction)callOutBtnClk:(id)sender {
 
    [self performSegueWithIdentifier:@"DetailBistroVC" sender:callOutBtnIndexPath];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    isCallOutBtnClick = NO;     // Added on 12-3
    
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    DetailBistroVC *VC = (DetailBistroVC*)[segue destinationViewController];
    VC.restaurantDetailDict = [venuesArray objectAtIndex:indexPath.row];
    isViewDidLayoutSubviews = NO;     // commented on 11-2-2016 , Added on 29-Feb , to again call venues API
    
    SharedObj.hasJoinedQueue = [[VC.restaurantDetailDict valueForKey:@"is_joined"] boolValue];
    
    SharedObj.isSeated = [[VC.restaurantDetailDict valueForKey:@"after_seated"] boolValue];
    
    SharedObj.restaurandID =  [[VC.restaurantDetailDict valueForKey:@"id"]intValue];    // added on 30-3
    
//    SharedObj.hasJoinedQueueStatus = [[VC.restaurantDetailDict valueForKey:@"is_joined"]intValue];  // Added on 29-3
    
    // Added on 3-2-2016 at 5:00 PM
    
    NSMutableDictionary *tmpDict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserDetails"] mutableCopy];
    [tmpDict setObject:[NSString stringWithFormat:@"%f",latitude] forKey:@"latitude"];
    [tmpDict setObject:[NSString stringWithFormat:@"%f",longitude] forKey:@"longitude"];
    
    [[NSUserDefaults standardUserDefaults]setObject:tmpDict forKey:@"UserDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([[VC.restaurantDetailDict valueForKey:@"slowmode"] isEqualToNumber:[NSNumber numberWithInt:0]]) {
        SharedObj.isSlowModeOn = false;
    }
    else{
        SharedObj.isSlowModeOn = true;
    }
    
    // till here
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"methodCalled" object:nil];
}

@end

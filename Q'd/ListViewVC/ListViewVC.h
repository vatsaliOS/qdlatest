//
//  ListViewVC.h
//  Q'd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ListViewVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTopConstraint;
@property (weak, nonatomic) IBOutlet MKMapView *mapViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewHeightConstraint;


@end



#import "NavigationClassVC.h"

@interface NavigationClassVC ()
{
    BOOL isViewPushed;
    UILabel *lblround;
}
@end

@implementation NavigationClassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
    
}

-(void)hideNavigationbar
{
    self.navigationController.navigationBarHidden = YES;
}

-(void)showNavigationbar
{
    self.navigationController.navigationBarHidden = NO;
}

-(void)setLeftBtn:(BOOL)isPushView {
    isViewPushed = isPushView;
    UIImage *leftImage;
    if (isViewPushed) {
         leftImage= [UIImage imageNamed:@"prev_arrow.png"];
    }
    else {
        leftImage= [UIImage imageNamed:@"close_icon.png"];
    }
    
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.bounds = CGRectMake( 10, 0, leftImage.size.width, leftImage.size.height );
    [btnLeft addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [btnLeft setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.navigationController.navigationBar.barTintColor = RGBCOLOR(58, 96, 153);
    self.navigationController.navigationBar.translucent = NO;
}
-(void)setLeftBtnWithImageName:(NSString *)imageName
{
    UIImage *leftImage;
    leftImage= [UIImage imageNamed:imageName];
    
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.bounds = CGRectMake( 0, 20, 85, 40);
    [btnLeft addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [btnLeft setImage:leftImage forState:UIControlStateNormal];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
    self.navigationItem.leftBarButtonItem = backButton;
    
//    self.navigationController.navigationBar.barTintColor = RGBCOLOR(243, 67, 75);
    self.navigationController.navigationBar.translucent = NO;
}

-(void)setLeftBtnWithImageName:(NSString *)imageName andLSelector:(SEL)LSelector{
    
    UIImage *leftImage;
    leftImage= [UIImage imageNamed:imageName];
    
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.bounds = CGRectMake( 0, 20, 85, 40 );

    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        btnLeft.titleLabel.font = [UIFont fontWithName:@"HindVadodara-Light" size:15.0f];
    }
    else if (IS_IPHONE_6 || IS_IPHONE_6P)
    {
        btnLeft.titleLabel.font = [UIFont fontWithName:@"HindVadodara-Light" size:18.0f];
    }
    [btnLeft addTarget:self action:LSelector forControlEvents:UIControlEventTouchUpInside];
    [btnLeft setImage:leftImage forState:UIControlStateNormal];
    [btnLeft setTitle:@"BACK" forState:UIControlStateNormal];
    [btnLeft setTitleEdgeInsets:UIEdgeInsetsMake(-2, -35, 0, 0)];
    [btnLeft setImageEdgeInsets:UIEdgeInsetsMake(0, -45, 0, 0)];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
    self.navigationItem.leftBarButtonItem = backButton;
//    self.navigationController.navigationBar.barTintColor = RGBCOLOR(243, 67, 75);
    self.navigationController.navigationBar.translucent = NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

//
-(void)setLeftBtnInListVIewVC:(NSString *)imageName andLSelector:(SEL)LSelector
{
    UIImage *leftImage;
    
    leftImage= [UIImage imageNamed:imageName];
    
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.bounds = CGRectMake( 0, 20, 48, 44 );
    [btnLeft addTarget:self action:LSelector forControlEvents:UIControlEventTouchUpInside];
//    [btnLeft setImage:leftImage forState:UIControlStateNormal];
    
    UIImageView *imgView = [[UIImageView alloc]init ];
    imgView.bounds = CGRectMake(10, 30, 29, 24);
    [imgView setImage:leftImage];
    [btnLeft addSubview:imgView];
    
//    [btnLeft setTitle:@"BACK" forState:UIControlStateNormal];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBar.barTintColor = RGBCOLOR(243, 67, 95);
    self.navigationController.navigationBar.translucent = NO;
}
//

-(void)setRightBtninListViewVCWithSelector:(SEL)RSelector
{
//    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    btnRight.bounds = CGRectMake(self.view.frame.size.width-90, 20, 82, 44);
//    [btnRight addTarget:self action:RSelector forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *rightBnt = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
//    self.navigationItem.rightBarButtonItem = rightBnt;
}

-(void)setRightBtn:(NSString*)imageName andRightSelector:(SEL)Rselector andWith:(BOOL)shouldDisplayRightBtn {
    if (shouldDisplayRightBtn) {
        UIImage *rightImage = [UIImage imageNamed:imageName];
        
        UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];

        btnRight.bounds = CGRectMake(20, 0, 23, 23);
        [btnRight addTarget:self action:Rselector forControlEvents:UIControlEventTouchUpInside];
        [btnRight setImage:rightImage forState:UIControlStateNormal];
        
        UIImage *imgNotiCount = [UIImage imageNamed:imageName];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, -10, 20, 20)];
        imgView.image =imgNotiCount;

        lblround=[[UILabel alloc]initWithFrame:CGRectMake(35, 0, 10, 10)];
        lblround.layer.cornerRadius=lblround.frame.size.width/2;
        lblround.clipsToBounds=YES;
        lblround.backgroundColor=RGBCOLOR(255, 54, 54);
        [btnRight addSubview:lblround];
        NSString *bagdecount=[NSString stringWithFormat:@"%@",XGET_VALUE(@"badgeCount")];
        if([bagdecount isEqualToString:@"0"])
            lblround.hidden=YES;
        else
            lblround.hidden=NO;
       // NSLog(@"Notification Counter is--->:%@",);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideShow:) name:@"NotificationCounter" object:nil];
        
        UIBarButtonItem *rightBnt = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
        self.navigationItem.rightBarButtonItem = rightBnt;
    }
}
-(void)setRightBtn1:(NSString*)imageName andRightSelector:(SEL)Rselector and1Button:(NSString*)imageName1 andRight1Selector:(SEL)R1Selector withNotiCount:(int)notiCount {
    UIImage *rightImage = [UIImage imageNamed:imageName];
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    btnRight.bounds = CGRectMake(20, 0, 23, 23);
    [btnRight addTarget:self action:Rselector forControlEvents:UIControlEventTouchUpInside];
    [btnRight setImage:rightImage forState:UIControlStateNormal];
    
    UIImage *imgNotiCount = [UIImage imageNamed:@"notification_icon.png"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, -10, 20, 20)];
    imgView.image =imgNotiCount;
    
    UILabel *lblCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [lblCount setTextColor:RGBCOLOR(236, 43, 92)];
    lblCount.text = [NSString stringWithFormat:@"%d",notiCount]; // use global varible
    lblCount.font = [UIFont fontWithName:@"Lato-Regular" size:15.0];
    lblCount.textAlignment = NSTextAlignmentCenter;
    [imgView addSubview:lblCount];
    [btnRight addSubview:imgView];
    
    UIBarButtonItem *rightBnt = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
    self.navigationItem.rightBarButtonItem = rightBnt;

    UIImage *rightImage1 = [UIImage imageNamed:imageName1];
    UIButton *btnRight1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btnRight1.bounds = CGRectMake(20, 0, 23, 23);
    [btnRight1 addTarget:self action:R1Selector forControlEvents:UIControlEventTouchUpInside];
    [btnRight1 setImage:rightImage1 forState:UIControlStateNormal];
    UIBarButtonItem *rightBnt1 = [[UIBarButtonItem alloc] initWithCustomView:btnRight1];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
    space.width = 25;
    
    NSArray *buttons = @[rightBnt, space, rightBnt1];
    self.navigationItem.rightBarButtonItems = buttons;
}

-(void)setTitle:(NSString *)title {
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 44)];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"HindVadodara-Semibold" size:22.0];
        titleView.text = title;
        titleView.textAlignment = NSTextAlignmentCenter;
        titleView.textColor = [UIColor whiteColor];
    }
    else {
        titleView.text = title;
    }
    self.navigationItem.titleView = titleView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backClick {
//    if (isViewPushed) {
//        PopToBack
//    }
//    else {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
}
#pragma mark - postNotification -
-(void)hideShow:(NSNotification*)notification
{
    NSString *strCounter;
    strCounter=[notification valueForKey:@"object"];
    if([[NSString stringWithFormat:@"%@",strCounter] isEqualToString:@"0"])
        lblround.hidden=YES;
    else
        lblround.hidden=NO;
}

@end

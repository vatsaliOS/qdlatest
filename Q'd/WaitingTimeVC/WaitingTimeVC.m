//
//  WaitingTimeVC.m
//  Qd
//
//  Created by SOTSYS028 on 15/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "WaitingTimeVC.h"
#import "MBProgressHUD.h"
#import "ListViewVC.h"

@interface WaitingTimeVC ()
{
    NSTimer *countDownTimer;
    int estimateWaitTime;
    
    BOOL isWaitTimeUpdated;
    
    NSTimer *everyMinuteTimer;
    __weak IBOutlet UIButton *yesBtn;
}
@property (weak, nonatomic) IBOutlet UILabel *estimateWaitTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *inQueueStaticLbl;

@end

@implementation WaitingTimeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // 19-3
    
    if (SharedObj.isSlowModeOn) {
        [yesBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    else
    {
         [yesBtn setImage:[UIImage imageNamed:@"PINKRIGHT"] forState:UIControlStateNormal];
    }
    
    //
    
    [self updateWaitTimeLabel];

    if (SharedObj.hasJoinedQueue) {
//    if (SharedObj.hasJoinedQueueStatus == 1) {
        inQueueLbl.text = [NSString stringWithFormat:@""];
        waitingTimeLbl.text = [NSString stringWithFormat:@""];
        restaurantnameLbl.text = [NSString stringWithFormat:@""];
        [self joinInQueueAPI:self.parameterDict];
    }
    else{
        SharedObj.hasJoinedQueue = true;
//        SharedObj.hasJoinedQueueStatus = 1;
        
        [restaurantImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.detailInformationDict valueForKey:@"image"]]]];
        restaurantnameLbl.text = [NSString stringWithFormat:@"%@",[self.detailInformationDict valueForKey:@"name"]];
        inQueueLbl.text = [NSString stringWithFormat:@"%@",[self.detailInformationDict valueForKey:@"in_the_queue"]];
        waitingTimeLbl.text = [NSString stringWithFormat:@"%@ minutes",[self.detailInformationDict valueForKey:@"estimate_wait_time"]];
        
        estimateWaitTime = [[self.detailInformationDict valueForKey:@"estimate_wait_time"] intValue];
        
        [self joinInQueueAPI:self.parameterDict];

    }
    
    SharedObj.joinInQueueParameterDict = self.parameterDict;
    
    [self setLeftBtnWithImageName:@"Back" andLSelector:@selector(backBtnClk:)];   // Use this when using custom Navigation bar
    
    self.navigationController.navigationBar.barTintColor = RGBCOLOR(255, 0, 78);
    self.navigationController.navigationBar.translucent = NO;
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        self.estimateWaitTimeLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:11.0];
        waitingTimeLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:12.0];
        restaurantnameLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:16.0];
        inQueueLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:12.0];
        self.inQueueStaticLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:11.0];
    }
    else if (IS_IPHONE_6 || IS_IPHONE_6P)
    {
       self.estimateWaitTimeLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:14.0];
        waitingTimeLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:22.0];
        restaurantnameLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:25.0];
        inQueueLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:22.0];
        self.inQueueStaticLbl.font = [UIFont fontWithName:@"HindVadodara-Regular" size:14.0];
    }
    
    UIButton *btnRight;
    btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btnRight.bounds = CGRectMake(0, 23, 90, 30);
    
    btnRight.layer.cornerRadius = 5.0f;
    [btnRight.titleLabel setFont:[UIFont fontWithName:@"HindVadodara-BOLD" size:14.0f]];
    [btnRight.titleLabel setTextAlignment:NSTextAlignmentRight];
    [btnRight setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRight setBackgroundColor:RGBCOLOR(246, 210, 79)];
    [btnRight addTarget:self action:@selector(leaveQueue:) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        btnRight.bounds = CGRectMake(0, 20, 80, 25);
        btnRight.titleLabel.font = [UIFont fontWithName:@"HindVadodara-BOLD" size:12.0];
        
    }
    else if (IS_IPHONE_6 || IS_IPHONE_6P)
    {
        btnRight.bounds = CGRectMake(0, 23, 95, 30);
        btnRight.titleLabel.font = [UIFont fontWithName:@"HindVadodara-BOLD" size:14.0];
    }
    
    //    [btnRight setImage:[UIImage imageNamed:@"join_btn@3x.png"] forState:UIControlStateNormal];
    //    [btnRight setBackgroundImage:[UIImage imageNamed:@"joinQueueBtn"] forState:UIControlStateNormal];
    //
    ////    [btnRight setImageEdgeInsets:UIEdgeInsetsMake(0, 35, 0, 0)];
    //
    ////    btnRight.layer.cornerRadius = 5.0f;
    //
    //    [btnRight addTarget:self action:@selector(joinQueueBtnClk:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
    self.navigationItem.rightBarButtonItem = backButton;
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    [btnRight setTitle:@"Leave Queue" forState:UIControlStateNormal];
    
    
    // Added on 16-3
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(applicationDidBecomeActiveCall) name:@"applicationDidBecomeActiveCall" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

#pragma mark - ApplicationDiDBecomeActiveNotification

- (void)applicationDidBecomeActiveCall{
    [self joinInQueueAPI:self.parameterDict];
}

#pragma mark - AutoUpdate Every Minute
-(void)updateWaitTimeLabel {
    
    if (!isWaitTimeUpdated) {
        if (estimateWaitTime > 0) {
            estimateWaitTime = estimateWaitTime-1;
            NSLog(@"estimate Wait Time == %d",estimateWaitTime);
            
            isWaitTimeUpdated = true;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                isWaitTimeUpdated = false;
            });
            
            if (estimateWaitTime>0) {
                waitingTimeLbl.text = [NSString stringWithFormat:@"%d minutes", estimateWaitTime];
                
                [everyMinuteTimer invalidate];
               everyMinuteTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(updateWaitTimeLabel) userInfo:nil repeats:YES];
            }
        }
    }
}

#pragma mark - LeaveQueue
- (IBAction)leaveQueue:(id)sender
{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Q'd"
                                          message:@"Are you sure you want to leave the queue?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Leave", @"Cancel action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                       
                                       // Leave Queue API
                                       
                                       NSMutableDictionary *Dict = [NSMutableDictionary new];
                                       [Dict setValue:[NSString stringWithFormat:@"%@",self.restaurantIDStr]forKey:@"restaurant_id"];
                                       [Dict setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
                                       
                                       [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurantsnew/leave_queue/",BASEURL] withParam:Dict withCompletion:^(NSDictionary *response, BOOL success){
                                           //        [MBProgressHUD hideHUDForView:self.view animated:YES];
                                           if (success)
                                           {
                                               // commented on 28-3
//                                               //            NSLog(@"response in success = %@",response);
//                                               for (UIViewController *VC in self.navigationController.viewControllers) {
//                                                   if ([VC isKindOfClass:[ListViewVC class]]) {
//                                                       [self.navigationController popToViewController:VC animated:YES];
//                                                   }
//                                               }
                                               // till here
                                               
                                               SharedObj.hasJoinedQueue = false;    // Added on 28-3
                                               SharedObj.isSeated = false;      // added on 29-3
                                               
                                               [self.navigationController popViewControllerAnimated:YES];
                                           }
                                           else
                                           {
                                               DisplayAlertControllerWithTitle(@"Something went wrong please try again", @"Q'd");
                                           }
                                       }];

                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"No", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
//                                   [self.navigationController popViewControllerAnimated:YES];

                               }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];

    
}

#pragma mark - JoinQueueAPI
-(void)joinInQueueAPI : (NSMutableDictionary*)Dict
{
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/join_queue/",BASEURL] withParam:Dict withCompletion:^(NSDictionary *response, BOOL success){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
//            NSLog(@"response in success = %@",response);
            //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
            
            SharedObj.hasJoinedQueue = true;
            
//            SharedObj.hasJoinedQueueStatus = 1;
            
            self.detailInformationDict = [response valueForKey:@"queue_details"];
            
            [restaurantImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.detailInformationDict valueForKey:@"image"]]]];
            restaurantnameLbl.text = [NSString stringWithFormat:@"%@",[self.detailInformationDict valueForKey:@"name"]];
            inQueueLbl.text = [NSString stringWithFormat:@"%@",[self.detailInformationDict valueForKey:@"in_the_queue"]];
            waitingTimeLbl.text = [NSString stringWithFormat:@"%@ minutes",[self.detailInformationDict valueForKey:@"estimate_wait_time"]];
            estimateWaitTime = [[self.detailInformationDict valueForKey:@"estimate_wait_time"] intValue];
            
            //            [self performSegueWithIdentifier:@"WaitingTimeVC" sender:response];
            
            //            if ([[self.restaurantDetailDict valueForKey:@"is_joined"] isEqualToNumber:[NSNumber numberWithInt:0]]) {
            //                [self.restaurantDetailDict setValue:[NSNumber numberWithInt:1] forKey:@"is_joined"];
            //            }
            
            [countDownTimer invalidate];
            countDownTimer = [NSTimer scheduledTimerWithTimeInterval:10*60 target:self selector:@selector(joinInQueueAPI:) userInfo:self.parameterDict repeats:YES];
        }
        else
        {
            DisplayAlertControllerWithTitle(@"Something went wrong please try again", @"Q'd");
        }
    }];
    
}


#pragma mark - Back Clicked
- (IBAction)backBtnClk:(id)sender {
    [everyMinuteTimer invalidate];
    [countDownTimer invalidate];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)yesBtnClick:(id)sender
{
    [everyMinuteTimer invalidate];
    [countDownTimer invalidate];

    if (SharedObj.isSlowModeOn) {
        SharedObj.isSlowModeOn = FALSE;
    }
    else
    {
        SharedObj.isSlowModeOn = TRUE;
    }
    
    [self updateSlowTimeAlertAPI:SharedObj.isSlowModeOn];
}

-(IBAction)slowAlertBtnClk:(id)sender
{
   
    
}

-(void)updateSlowTimeAlertAPI :(BOOL)slowTimeAlert
{
    //    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%@",self.restaurantIDStr]forKey:@"restaurant_id"];
    [Dict setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
    [Dict setValue:[NSNumber numberWithBool:slowTimeAlert] forKey:@"is_show_time_alert"];
    
    NSMutableDictionary *mainDict = [NSMutableDictionary new];
    [mainDict setObject:Dict forKey:@"userslowtimealert"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/update_slow_time_alerts/",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
        //        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            [self.navigationController popViewControllerAnimated:YES];
//            SharedObj.isSlowModeOn = true;
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
//            SharedObj.isSlowModeOn = true;
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  WaitingTimeVC.h
//  Qd
//
//  Created by SOTSYS028 on 15/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaitingTimeVC : NavigationClassVC
{
    IBOutlet UIImageView *restaurantImgView;
    IBOutlet UILabel *restaurantnameLbl;
    IBOutlet UILabel *waitingTimeLbl;
    IBOutlet UILabel *inQueueLbl;
}
@property(nonatomic,retain) NSMutableDictionary *detailInformationDict;
@property(nonatomic,retain) NSMutableDictionary *parameterDict;
@property(nonatomic,retain) NSString *restaurantIDStr;

@end
